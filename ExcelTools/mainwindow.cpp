#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QAxObject>
#include <iostream>
#include <QDebug>
#include <QVector>
#include <QList>
#include <QMap>
#include <QFile>
#include <QStandardPaths>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QString strFile = QFileDialog::getOpenFileName(this,QStringLiteral("选择Excel文件"),"",tr("Exel file(*.xls *.xlsx)"));

     if( strFile.isEmpty() )
    {
         return;
    }

    QAxObject excel("Excel.Application"); //加载Excel驱动

    excel.setProperty("Visible", false); //不显示Excel界面，如果为true会看到启动的Excel界面

    QAxObject* pWorkBooks = excel.querySubObject("WorkBooks");

    pWorkBooks->dynamicCall("Open (const QString&)", strFile);//打开指定文

    QAxObject* pWorkBook = excel.querySubObject("ActiveWorkBook");

    QAxObject* pWorkSheets = pWorkBook->querySubObject("Sheets");//获取工作表

    int nSheetCount = pWorkSheets->property("Count").toInt();  //获取工作表的数目
    //qDebug()<<nSheetCount<<endl;

    if(nSheetCount > 0)
    {
        QAxObject* pWorkSheet = pWorkBook->querySubObject("Sheets(int)", 1);//获取第一张表

        QAxObject* usedrange = pWorkSheet->querySubObject("Usedrange");

        int introw = usedrange->querySubObject("Rows")->property("Count").toInt();
        qDebug()<<introw<<endl;

        int inta = usedrange->querySubObject("Columns")->property("Count").toInt();
        qDebug()<<inta<<endl;

        if( 10000 > introw )
        {
            read( pWorkSheet,introw );
        }
        else
        {
            readbig( pWorkSheet,introw );
        }

      }
        pWorkBooks->dynamicCall("Close()");

}

MainWindow::~MainWindow()
{
    delete ui;
}

void
MainWindow::read(
         QAxObject*     pWorkSheet,
         int            introw
        )
{
    QStringList strList;
    QStringList strList2;



    for( int i = 1; i <= introw; i++ )    //从1开始，去掉标题
    {
            QString str  = pWorkSheet->querySubObject("Cells(int,int)",i ,3)->dynamicCall(("Value2()")).value<QString>();
            QString str2 = pWorkSheet->querySubObject("Cells(int,int)",i ,4)->dynamicCall(("Value2()")).value<QString>();

            strList.append(str);
            strList2.append(str2);
    }

    qDebug()<<strList.length()<<endl;

    QMap<QString,int> map;

    for( int i = 1; i < strList.length(); i++ )
    {
        if( map.isEmpty() )
        {
            map.insert( strList.at(i), 1 );
        }
        else
        {
            QMap<QString,int>::Iterator it = map.find( strList.at(i) );
            if( it != map.end() )
            {
                int num = it.value();
                num++;
                it.value() = num;
            }
            else
            {
                map.insert( strList.at(i), 1 );
            }
        }

    }

    QMap<QString,int>::Iterator it1 = map.begin();
    QMap<QString,int> map2;

    for( ;it1 != map.end(); it1++ )
    {

        map2.insert( it1.key(),0 );
        qDebug()<<it1.key()<<" "<<it1.value()<<endl;

    }

    //qDebug()<<strList.length()<<" "<<strList2.length()<<endl;
    QStringList strComName;
    QMap<QString,int>::Iterator it2 = map2.begin();
    int n = 0;


    for( ; it2 != map2.end(); it2++ )
    {
       strComName.clear();
       for( int i = 1; i < strList2.length(); i++ )
       {
           if( strList.at(i) == it2.key() )
           {
               if( !strComName.contains(strList2.at(i)) )
               {
                   n = it2.value();
                   n++;
                   it2.value() = n;

                   strComName.append(strList2.at(i));
                }
            }
        }
    }

    it2 = map2.begin();
    for( ; it2 != map2.end(); it2++ )
    {
        qDebug()<<it2.key()<<" "<<it2.value()<<endl;
    }
    qDebug()<<endl;

    QMap<QString,QString> mapend;

   qDebug()<<map.count()<<endl;

   QStringList strList3;
   QMap<QString,int>::Iterator it5 = map2.begin();

   for( ; it5 != map2.end(); it5++ )
   {
       QString zhuan = QString::number( it5.value(), 10 );
       strList3.append( zhuan );
   }

   QStringList strList4, strList7;
   QMap<QString,int>::Iterator it6 = map.begin();
   for( ; it6 != map.end(); it6++ )
   {
       QString zhuan = QString::number( it6.value(), 10 );
       strList4.append( zhuan );
       strList7.append( it6.key() );
   }

   QStringList strList5;
   for( int i = 0; i < strList3.length(); i++ )
   {
       QString zhuan = strList4.at(i) + "(" + strList3.at(i) + ")";
       strList5.append(zhuan);
   }

   for( int i = 0; i < strList5.length(); i++ )
   {
       mapend.insert( strList7.at(i), strList5.at(i) );
   }

    QMap<QString,QString>::Iterator it4 = mapend.begin();

    for( ; it4 != mapend.end(); it4++ )
    {
        qDebug()<<it4.key()<<" "<<it4.value()<<endl;
    }

    //shuchu(map);
}



void
MainWindow::readbig(
         QAxObject* pWorkSheet,
         int    introw
        )
{
    QMap<QString,int> map;
    QMap<QString,int> map2;

    QString str;
    QString str2;

    for( int i = 2; i <= introw; i++ )    //从1开始，去掉标题
    {
            str  = pWorkSheet->querySubObject("Cells(int,int)",i ,3)->dynamicCall(("Value2()")).value<QString>();
            //str2 = pWorkSheet->querySubObject("Cells(int,int)",i ,4)->dynamicCall(("Value2()")).value<QString>();

            if( map.isEmpty() )
            {
                map.insert( str, 1 );
            }
            else
            {
                QMap<QString,int>::Iterator it = map.find( str );
                if( it != map.end() )
                {
                    int num = it.value();
                    num++;
                    it.value() = num;
                }
                else
                {
                    map.insert( str, 1 );
                }
            }

    }


    QMap<QString, int>::Iterator it2 = map.begin();
    for( ; it2 != map.end(); it2++ )
    {
        qDebug()<<it2.key()<<" "<<it2.value()<<endl;
    }

    //shuchu(map);
}



void
MainWindow::shuchu(
        QMap<QString,int> map
        )
{

    QString desktop_path = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);//获取桌面路径
    QString excel_path_1 = desktop_path + "/ldl.xlsx"; //设置文件路径、名、格式

    if(!excel_path_1.isEmpty()){
        QAxObject *excel = new QAxObject(this);
        excel->setControl("Excel.Application");//连接Excel控件
        excel->dynamicCall("SetVisible (bool Visible)","false");//不显示窗体
        excel->setProperty("DisplayAlerts", true);//不显示任何警告信息。如果为true那么在关闭是会出现类似“文件已修改，是否保存”的提示

        QAxObject *workbooks = excel->querySubObject("WorkBooks");//获取工作簿集合
        workbooks->dynamicCall("Add");//新建一个工作簿
        QAxObject *workbook = excel->querySubObject("ActiveWorkBook");//获取当前工作簿
        QAxObject *worksheets = workbook->querySubObject("Sheets");//获取工作表集合
        QAxObject *worksheet = worksheets->querySubObject("Item(int)",1);//获取工作表集合的工作表1，即sheet1

//        //标题行
//        QAxObject *cell;
//        cell=worksheet->querySubObject("Cells(int,int)", 1, 1);
//        cell->dynamicCall("SetValue(const QString&)", "ldlldldldlldldldldldldldldl");
//        cell->querySubObject("Font")->setProperty("Size", 11);
//        //合并标题行
//        QString cellTitle;
//        cellTitle.append("A1:");
//        cellTitle.append(QChar(4 + 'A'));
//        cellTitle.append(QString::number(1));
//        QAxObject *range = worksheet->querySubObject("Range(const QString&)", cellTitle);
//        range->setProperty("WrapText", true);
//        range->setProperty("MergeCells", true);
//        range->setProperty("HorizontalAlignment", -4108);//xlCenter
//        range->setProperty("VerticalAlignment", -4108);//xlCenter
        QAxObject *range = NULL;

        QAxObject *cellA,*cellB,*cellC,*cellD,*cellE;

        //设置标题
        int cellrow=1;

        QString A="A"+QString::number(cellrow);
        QString B="B"+QString::number(cellrow);
        QString C="C"+QString::number(cellrow);
        QString D="D"+QString::number(cellrow);
        QString E="E"+QString::number(cellrow);
        QString F="F"+QString::number(cellrow);
        QString G="G"+QString::number(cellrow);
        QString H="H"+QString::number(cellrow);
        QString I="I"+QString::number(cellrow);
        QString J="J"+QString::number(cellrow);
        QString K="K"+QString::number(cellrow);
        QString L="L"+QString::number(cellrow);
        QString M="M"+QString::number(cellrow);
        QString N="N"+QString::number(cellrow);

        QStringList strList;
        strList.push_back(A);
        strList.push_back(B);
        strList.push_back(C);
        strList.push_back(D);
        strList.push_back(E);
        strList.push_back(F);
        strList.push_back(G);
        strList.push_back(H);
        strList.push_back(I);
        strList.push_back(J);
        strList.push_back(K);
        strList.push_back(L);
        strList.push_back(M);
        strList.push_back(N);


        int iSize = 0;
        QMap<QString,int>::iterator it = map.begin();
        for( ; it != map.end(); it++ )
        {
            iSize++;
        }
        qDebug()<<iSize<<endl;

        it = map.begin();

        for( int i = 0; i < iSize; i++ )
        {
            if( it == map.end() )
                break;

            cellA = worksheet->querySubObject("Range(QVariant, QVariant)",strList.at(i));

            cellA->dynamicCall("SetValue(const QVariant&)",QVariant(it.key()));
            it++;

        }

        for( int i = 0; i < 1; i++ ){
            QString A="A"+QString::number(i+2);//设置要操作的单元格，如A1
            QString B="B"+QString::number(i+2);
            QString C="C"+QString::number(i+2);
            QString D="D"+QString::number(i+2);
            QString E="E"+QString::number(i+2);
            cellA = worksheet->querySubObject("Range(QVariant, QVariant)",A);//获取单元格
            cellB = worksheet->querySubObject("Range(QVariant, QVariant)",B);
            cellC=worksheet->querySubObject("Range(QVariant, QVariant)",C);
            cellD=worksheet->querySubObject("Range(QVariant, QVariant)",D);
            cellE=worksheet->querySubObject("Range(QVariant, QVariant)",E);

            QMap<QString,int>::iterator it2 = map.begin();
            cellA->dynamicCall("SetValue(const QVariant&)",QVariant(it2.value()));//设置单元格的值
            range = worksheet->querySubObject("Range(const QString&)", A); //设置A单元格元素居中显示
            range->setProperty("HorizontalAlignment", -4108);//xlCenter
            range->setProperty("VerticalAlignment", -4108);//xlCenter

            it2++;
            cellB->dynamicCall("SetValue(const QVariant&)",it2.value());
            range = worksheet->querySubObject("Range(const QString&)", B); //设置B单元格元素居中显示
            range->setProperty("HorizontalAlignment", -4108);//xlCenter
            range->setProperty("VerticalAlignment", -4108);//xlCenter

            it2++;
            cellC->dynamicCall("SetValue(const QVariant&)",QVariant(it2.value()));
            range = worksheet->querySubObject("Range(const QString&)", C); //设置C单元格元素居中显示
            range->setProperty("HorizontalAlignment", -4108);//xlCenter
            range->setProperty("VerticalAlignment", -4108);//xlCenter

            it2++;
            cellD->dynamicCall("SetValue(const QVariant&)",QVariant(it2.value()));
            range = worksheet->querySubObject("Range(const QString&)", D); //设置D单元格元素居中显示
            range->setProperty("HorizontalAlignment", -4108);//xlCenter
            range->setProperty("VerticalAlignment", -4108);//xlCenter

            it2++;
            cellE->dynamicCall("SetValue(const QVariant&)",QVariant(it2.value()));
            range = worksheet->querySubObject("Range(const QString&)", E); //设置E单元格元素居中显示
            range->setProperty("HorizontalAlignment", -4108);//xlCenter
            range->setProperty("VerticalAlignment", -4108);//xlCenter
        }

        workbook->dynamicCall("SaveAs(const QString&)",QDir::toNativeSeparators(excel_path_1));//保存至filepath，注意一定要用QDir::toNativeSeparators将路径中的"/"转换为"\"，不然一定保存不了。
        workbook->dynamicCall("Close()");//关闭工作簿
        excel->dynamicCall("Quit()");//关闭excel

        if( NULL != excel )
        {
            delete excel;
            excel=NULL;
        }

        QMessageBox::information(NULL,"","ldl输出完成");
    }

}
