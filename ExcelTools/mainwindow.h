#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>
#include <QAxObject>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void shuchu( QMap<QString,int> map );
    void read(  QAxObject* pWorkSheets, int introw );
    void readbig( QAxObject* pWorkSheets, int introw );

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
