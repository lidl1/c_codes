#-------------------------------------------------
#
# Project created by QtCreator 2020-12-14T10:20:08
#
#-------------------------------------------------

QT       += core gui
QT       += network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 1214
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    tcpClientSocket.cpp \
    tcpServer.cpp

HEADERS  += dialog.h \
    tcpClientSocket.h \
    tcpServer.h

FORMS    += dialog.ui
