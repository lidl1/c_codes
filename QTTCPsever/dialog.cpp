#include "dialog.h"
#include "ui_dialog.h"
#include <windows.h>

Dialog::Dialog( QWidget *parent, Qt::WindowFlags f ) :
    QDialog( parent, f ),
    ui(new Ui::Dialog)
{
    ui->setupUi( this );
    setWindowTitle( tr( "Server" ) );
    do
    {
        pContentListWidget = new QListWidget;
        if( NULL == pContentListWidget )
        {
            qDebug()<<"pContentListWidget error";
            break;
        }

        pPortLabel = new QLabel( tr( "端口：" ) );
        if( NULL == pPortLabel )
        {
            qDebug()<<"pPortLabel error";
            break;
        }

        pPortLineEdit = new QLineEdit;
        if( NULL == pPortLineEdit )
        {
            qDebug()<<"pPortLineEdit error";
            break;
        }

        pCreateBtn = new QPushButton( tr( "创造聊天室" ) );
        if( NULL == pCreateBtn )
        {
            qDebug()<<"pCreateBtn error";
            break;
        }

        pMainLayout = new QGridLayout( this );
        if( NULL == pMainLayout )
        {
            qDebug()<<"pMainLayout error";
            break;
        }

        pMainLayout->addWidget( pContentListWidget, 0 ,0, 1, 2 );
        pMainLayout->addWidget( pPortLabel, 1, 0 );
        pMainLayout->addWidget( pPortLineEdit, 1, 1 );
        pMainLayout->addWidget( pCreateBtn, 2, 0, 1, 2 );

        bStatus = false;
        iPort = 8010;
        pPortLineEdit->setText( QString::number( iPort ) );

        connect( pCreateBtn, SIGNAL( clicked() ), this, SLOT( slotCreateServer() ) );

    }while( FALSE );
}

Dialog::~Dialog()
{
    delete ui;
}

void
Dialog::slotCreateServer(
    )
{
    if( !bStatus )
    {
        QString str = pPortLineEdit->text();
        iPort = str.toInt();
        qDebug()<<iPort;

        pServer = new tcpServer( this,iPort );
        if( NULL == pServer )
        {
            return;
        }

        connect( pServer, SIGNAL( updateServer( QString, int ) ), this, SLOT( updateServer( QString, int ) ) );

        pCreateBtn->setText( tr( "断开" ) );
        bStatus = true;
    }
    else
    {
        pServer->close();
        pServer->deleteLater();
        pCreateBtn->setText( tr( "创建聊天室" ) );
        bStatus = false;
    }
}

void
Dialog::updateServer(
   IN QString      strMsg,
   IN int          iLength
    )
{
    //qDebug()<<"server error";
    pContentListWidget->addItem( strMsg.left( iLength ) );
}
