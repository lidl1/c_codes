#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QListWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include "tcpServer.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog( QWidget* parent = 0, Qt::WindowFlags f = 0 );
    ~Dialog();

private:
    Ui::Dialog *ui;
    QListWidget* pContentListWidget;
    QLabel* pPortLabel;
    QLineEdit* pPortLineEdit;
    QPushButton* pCreateBtn;
    QGridLayout* pMainLayout;
    int iPort;
    bool bStatus;
    tcpServer* pServer;

public slots:
    void slotCreateServer();
    void updateServer( QString, int );
};

#endif // DIALOG_H
