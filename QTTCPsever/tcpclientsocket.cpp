#include "tcpClientSocket.h"

tcpClientSocket::tcpClientSocket( QObject* parent )
{
    connect( this, SIGNAL( readyRead() ), this, SLOT( dataReceived() ) );
    connect( this, SIGNAL( disconnected() ), this,SLOT( slotDisconnected()) );
}

void
tcpClientSocket::dataReceived(
    )
{
    while( bytesAvailable() > 0 )
    {
        int iLength = bytesAvailable();

        //char cBuf[1024];
        QByteArray byData;
        byData = readAll();
        //read( byData.data() ,iLength );
        qDebug()<<byData.size();
        QString strMsg = QString::fromLocal8Bit( byData.data() ) ;

        emit updateClients( strMsg, iLength );
    }
}

void
tcpClientSocket::slotDisconnected(
    )
{
    emit disconnected( this->socketDescriptor() );
}
