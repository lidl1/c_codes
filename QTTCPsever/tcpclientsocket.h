#ifndef TCPCLIENTSOCKET_H
#define TCPCLIENTSOCKET_H

#include <QTcpSocket>
#include <QObject>

class tcpClientSocket : public QTcpSocket
{
    Q_OBJECT
public:
    tcpClientSocket( QObject* parent = 0 );

signals:
    void updateClients( QString, int );
    void disconnected( int );

protected slots:
    void dataReceived();
    void slotDisconnected();

};

#endif // TCPCLIENTSOCKET_H
