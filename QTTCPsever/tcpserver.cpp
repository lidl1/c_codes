#include "tcpServer.h"
#include "dialog.h"
#include <windows.h>

tcpServer::tcpServer( QObject* parent, int iPort )
{
    listen( QHostAddress::Any,iPort );
}

void
tcpServer::incomingConnection(
    IN int        socketDescriptor
    )
{
    tcpClientSocket* pTcpClientSocket = new tcpClientSocket( this );
    if( NULL == pTcpClientSocket )
    {
        return;
    }

    pTcpClientSocket->setSocketDescriptor( socketDescriptor );
    tcpClientSocketList.append( pTcpClientSocket );
    connect( pTcpClientSocket, SIGNAL( updateClients(QString,int)), this, SLOT( updateClients( QString,int ) )  );
    connect( pTcpClientSocket, SIGNAL( disconnected( int )), this, SLOT( slotDisconnected( int ) )  );
}

void
tcpServer::updateClients(
    IN QString      strMsg,
    IN int          iLength
    )
{
    emit updateServer( strMsg, iLength );
    for( int i = 0; i < tcpClientSocketList.count(); i++ )
    {
        QTcpSocket* pItem = tcpClientSocketList.at( i );

        if( pItem->write( strMsg.toLocal8Bit(), iLength ) != iLength )
        {
            continue;
        }
    }

}

void
tcpServer::slotDisconnected(
    IN int     iDescriptor
    )
{
    for( int i = 0; i < tcpClientSocketList.count(); i++ )
    {
        QTcpSocket* pItem = tcpClientSocketList.at( i );
        if( pItem->socketDescriptor() == iDescriptor )
        {
            tcpClientSocketList.removeAt( i );
            return;
        }
    }
    return;
}
