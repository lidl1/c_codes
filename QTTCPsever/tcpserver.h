#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QTcpServer>
#include <QObject>
#include "tcpClientSocket.h"

class tcpServer : public QTcpServer
{
    Q_OBJECT
public:
    tcpServer( QObject* parent = 0, int port = 0 );
    QList<tcpClientSocket*> tcpClientSocketList;

signals:
    void updateServer( QString, int );

public slots:
    void updateClients( QString, int );
    void slotDisconnected( int );

protected:
    void incomingConnection( int socketDescriptor );
};

#endif // TCPSERVER_H
