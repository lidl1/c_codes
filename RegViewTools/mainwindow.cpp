#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <iostream>
#include <tchar.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <sys/stat.h>
#include <process.h>
#include <tlhelp32.h>
#include <afxres.h>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QString>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QCoreApplication>
#include <QStringList>
#include <psapi.h>
#include <wchar.h>
#include <QIcon>
#include <QSettings>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("注册表编辑器");
    QIcon icon;
    icon.addPixmap(QPixmap(":/5213.PNG"));
    setWindowIcon(icon);
    ui->treeWidget->setHeaderLabel("注册表");
    proot = new QTreeWidgetItem(QStringList()<<"计算机");
    if( proot == NULL )
    {
        qDebug()<<"error";
    }
    icon.addPixmap(QPixmap(":/5214.PNG"));
    proot->setIcon(0,icon);
    ui->treeWidget->addTopLevelItem(proot);

    QTreeWidgetItem* phkey1 = new QTreeWidgetItem(QStringList()<<"HKEY_CLASSES_ROOT");
    if( phkey1 == NULL )
    {
        qDebug()<<"error";
    }
    QIcon icon2;
    icon2.addPixmap(QPixmap(":/132.PNG"));
    phkey1->setIcon(0,icon2);
    proot->addChild(phkey1);

    QString strReg = "HKEY_CLASSES_ROOT";
    QSettings * setting = new QSettings(strReg,QSettings::NativeFormat);
    if( setting == NULL )
    {
        qDebug()<<"error";
    }
    QStringList appList = setting->childGroups();
    for( INT i=0; i<appList.count(); i++ )
    {
        prootchild = new QTreeWidgetItem(QStringList()<<appList.at(i));
        if( prootchild == NULL )
        {
            qDebug()<<"error";
        }
        prootchild->setIcon(0,icon2);
        phkey1->addChild(prootchild);

       /* QString str1 = appList.at(i);
        WCHAR wch[100] = { 0 };
        str1.toWCharArray(wch);
        //CHAR* ch;
        QByteArray bytearray = str1.toLatin1();
        ch = bytearray.data();

        size_t len = strlen( ch )+1;
        size_t converted = 0;
        WCHAR *wch = (WCHAR*)malloc( len*sizeof(WCHAR) );
        //mbstowcs_s( &converted,wch,len,ch,_TRUNCATE );
        HKEY hMainKey1 = HKEY_CLASSES_ROOT;
        search(wch,prootchild,hMainKey1);*/
    }



    QTreeWidgetItem* phkey2 = new QTreeWidgetItem(QStringList()<<"HKEY_CURRENT_USER");
    if( phkey2 == NULL )
    {
        qDebug()<<"error";
    }
    phkey2->setIcon(0,icon2);
    proot->addChild(phkey2);

    strReg = "HKEY_CURRENT_USER";
    setting = new QSettings(strReg,QSettings::NativeFormat);
    if( setting == NULL )
    {
        qDebug()<<"error";
    }
    appList = setting->childGroups();
    for( INT i=0; i<appList.count(); i++ )
    {
        userchild = new QTreeWidgetItem(QStringList()<<appList.at(i));
        if( userchild == NULL )
        {
            qDebug()<<"error";
        }
        userchild->setIcon(0,icon2);
        phkey2->addChild(userchild);

        QString str2 = appList.at(i);
        WCHAR wch2[100] = { 0 };
        str2.toWCharArray(wch2);
        HKEY hMainKey2 = HKEY_CURRENT_USER;
        search(wch2,userchild,hMainKey2);
    }



    QTreeWidgetItem* phkey3 = new QTreeWidgetItem(QStringList()<<"HKEY_LOCAL_MACHINE");
    if( phkey3 == NULL )
    {
        qDebug()<<"error";
    }

    phkey3->setIcon(0,icon2);
    proot->addChild(phkey3);

    strReg = "HKEY_LOCAL_MACHINE";
    setting = new QSettings(strReg,QSettings::NativeFormat);
    if( setting == NULL )
    {
        qDebug()<<"error";
    }
    appList = setting->childGroups();
    for( INT i=0; i<appList.count(); i++ )
    {
        machinechild = new QTreeWidgetItem(QStringList()<<appList.at(i));
        if( machinechild == NULL )
        {
            qDebug()<<"error";
        }
        machinechild->setIcon(0,icon2);
        phkey3->addChild(machinechild);

        /*QString str3 = appList.at(i);
        WCHAR wch3[100] = { 0 };
        str3.toWCharArray(wch3);
        HKEY hMainKey3 = HKEY_LOCAL_MACHINE;
        search(wch3,machinechild,hMainKey3);*/
    }



    QTreeWidgetItem* phkey4 = new QTreeWidgetItem(QStringList()<<"HKEY_USERS");
    if( phkey4 == NULL )
    {
        qDebug()<<"error";
    }
    phkey4->setIcon(0,icon2);
    proot->addChild(phkey4);

    strReg = "HKEY_USERS";
    setting = new QSettings(strReg,QSettings::NativeFormat);
    if( setting == NULL )
    {
        qDebug()<<"error";
    }
    appList = setting->childGroups();
    for( INT i=0; i<appList.count(); i++ )
    {
        userschild = new QTreeWidgetItem(QStringList()<<appList.at(i));
        if( userschild == NULL )
        {
            qDebug()<<"error";
        }
        userschild->setIcon(0,icon2);
        phkey4->addChild(userschild);

     /*   QString str4 = appList.at(i);
        WCHAR wch4[100] = { 0 };
        str4.toWCharArray(wch4);
        HKEY hMainKey4 = HKEY_USERS;
        search(wch4,userschild,hMainKey4);*/
    }



    QTreeWidgetItem* phkey5 = new QTreeWidgetItem(QStringList()<<"HKEY_CURRENT_CONFIG");
    if( phkey5 == NULL )
    {
        qDebug()<<"error";
    }
    phkey5->setIcon(0,icon2);
    proot->addChild(phkey5);

    strReg = "HKEY_CURRENT_CONFIG";
    setting = new QSettings(strReg,QSettings::NativeFormat);
    if( setting == NULL )
    {
        qDebug()<<"error";
    }
    appList = setting->childGroups();
    for( INT i=0; i<appList.count(); i++ )
    {
        QTreeWidgetItem* configchild = new QTreeWidgetItem(QStringList()<<appList.at(i));
        if( configchild == NULL )
        {
            qDebug()<<"error";
        }
        configchild->setIcon(0,icon2);
        phkey5->addChild(configchild);
    }
    QTreeWidgetItem* configchild = new QTreeWidgetItem(QStringList()<<"Software");
    if( configchild == NULL )
    {
        qDebug()<<"error";
    }
    configchild->setIcon(0,icon2);
    phkey5->addChild(configchild);

    connect( ui->treeWidget,SIGNAL( itemDoubleClicked(QTreeWidgetItem*,int) ),this,SLOT( type(QTreeWidgetItem*,int) ) );
}


VOID
MainWindow::
search(
   IN WCHAR*			  ppath,
   IN QTreeWidgetItem*    pfather,
   IN HKEY                hMainKey
       )
{
    HKEY hSubKey;
    HKEY hSsubKey;
    WCHAR wchpath[500] = { 0 };
    wcscpy( wchpath,ppath );
    WCHAR wchpath1[500] = { 0 };
    RegOpenKeyEx( hMainKey, wchpath, 0, KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE|KEY_WOW64_64KEY, &hSubKey );

    DWORD dwSubKeyCount = 0;
    DWORD dwSsubKeyCount = 0;
    DWORD dwKeyvalueCount = 0;
    INT i1 = 0;
    RegQueryInfoKey( hSubKey, NULL, NULL, NULL, &dwSubKeyCount, NULL, NULL, &dwKeyvalueCount, NULL, NULL, NULL, NULL );

    WCHAR tcKeyname[300] = { 0 };
    DWORD dwKeyNameSize = 300;

    char * pl = "\\";
    size_t len1 = strlen(pl)+1;
    size_t converted1 = 0;
    WCHAR *pwch2 = (WCHAR*)malloc(len1*sizeof(WCHAR));
    mbstowcs_s(&converted1,pwch2,len1,pl,_TRUNCATE);

    QString asd = QString::fromWCharArray( wchpath );
    qDebug() << asd << endl;

    for( INT i = 0; i < dwSubKeyCount; i++ )
    {
        wcscpy( wchpath1, wchpath );
        RegEnumKeyEx( hSubKey, i, tcKeyname, &dwKeyNameSize, NULL, NULL, NULL, NULL );
        QString str1 = QString::fromWCharArray( tcKeyname );
        QIcon icon1;
        icon1.addPixmap(QPixmap(":/132.PNG"));

        tree = new QTreeWidgetItem(QStringList()<<str1);
        if( tree == NULL )
        {
            qDebug()<<"error";
        }

        tree->setIcon( 0,icon1 );
        pfather->addChild( tree );
        //qDebug() << str1 << endl;

        dwKeyNameSize = 128;
        wcscat( wchpath1, pwch2 );
        wcscat( wchpath1, tcKeyname );

        RegOpenKeyEx( hMainKey, wchpath1, 0, KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE|KEY_WOW64_64KEY, &hSsubKey );
        RegQueryInfoKey( hSsubKey, NULL, NULL, NULL, &dwSsubKeyCount, NULL, NULL, &dwKeyvalueCount,NULL, NULL, NULL, NULL );
        if( dwSsubKeyCount != 0 )
        {
            search( wchpath1 ,tree,hMainKey);
        }

        *wchpath1 = { 0 };
        i1++;
    }
    free(wchpath);
    free(wchpath1);
    free(pwch2);
    return;
}

void
MainWindow::
type(
    IN QTreeWidgetItem    *item,
    IN INT                 n
    )

{
    ui->tableWidget->clear();
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setRowCount(50);
    ui->tableWidget->setHorizontalHeaderLabels( QStringList()<<"名称"<<"类型"<<"数据" );

    QString str = item->text(n);
    QTreeWidgetItem * a;
    QString str3;
    QString strsave;
    QStringList list;

    HKEY hMainKey = HKEY_CURRENT_USER;
    HKEY hSsubKey;
    DWORD dwKeyvalueCount = 0;
    DWORD dwSsubKeyCount = 0;
    DWORD ty;
    ui->tableWidget->setColumnWidth(2,300);
    ui->tableWidget->setColumnWidth(0,200);

    do{
        a=item->parent( );
        if( a == proot )
        {
           break;
        }
        str3=a->text( n );
        list.append( str3 );
        item = a;
    } while( TRUE );
    strsave = str3;
    QString str4 = "\\";
    for( int j = list.count()-2; j>=0; j-- )
    {
        str3.append( str4 );
        str3.append( list.at(j) );
    }
    if( list.count()>=1 )
    {
        str3.append( str4 );
    }
    str3.append( str );
    qDebug()<<str3;

    QString str8 = str3.right(str3.length()-strsave.length()-1);
    qDebug()<<str8;
    const WCHAR *chpaath1 = reinterpret_cast<const WCHAR*>( str8.utf16() );
    QString str5 = QString::fromWCharArray(chpaath1);
    qDebug()<<str5;

    RegOpenKeyEx( hMainKey, chpaath1, 0, KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE|KEY_WOW64_64KEY, &hSsubKey );
    RegQueryInfoKey( hSsubKey, NULL, NULL, NULL, &dwSsubKeyCount, NULL, NULL, &dwKeyvalueCount,NULL, NULL, NULL, NULL );
    qDebug()<<dwKeyvalueCount;

    for( INT i = 0; i < dwKeyvalueCount; i++ )
    {
        WCHAR tcKeyname1[128] = { 0 };
        DWORD dwKeyNameSize1 = 128;
        WCHAR tcKeyvalue[500] = { 0 };
        DWORD dwKeyvalueSize1 = 500;

        RegEnumValue( hSsubKey, i, tcKeyname1, &dwKeyNameSize1, NULL, &ty,
                      (LPBYTE)tcKeyvalue, &dwKeyvalueSize1);
        QString str1 = QString::fromWCharArray(tcKeyname1);
        qDebug()<<str1;

        QIcon icon;
        table = new QTableWidgetItem(str1);

        if( ty == REG_SZ )
        {
            icon.addPixmap(QPixmap(":/521.PNG"));

            table->setIcon(icon);
            ui->tableWidget->setItem(i,0,table);

            QString ty1 = "REG_SZ";
            table1 = new QTableWidgetItem(ty1);
            ui->tableWidget->setItem(i,1,table1);

            QString str7 = QString::fromWCharArray( tcKeyvalue );
            //qDebug()<<tcKeyvalue<<str7;

            QTableWidgetItem *item2 = new QTableWidgetItem( str7 );
            ui->tableWidget->setItem( i,2,item2 );
        }



        else if( ty == REG_DWORD )
        {
          icon.addPixmap(QPixmap(":/5212.PNG"));
          table->setIcon(icon);
          ui->tableWidget->setItem(i,0,table);

          QString ty1 = "REG_DWORD";
          table1 = new QTableWidgetItem(ty1);
          ui->tableWidget->setItem(i,1,table1);

          DWORD dwret,ty2,dwret2;
          DWORD dwKeyvalueSize2 = 500;
          WCHAR tcKeyname3[128] = { 0 };
          DWORD dwKeyNameSize3 = 128;
          RegEnumValue( hSsubKey, i,tcKeyname3, &dwKeyNameSize3, NULL, &ty2,
                        (LPBYTE)&dwret, &dwKeyvalueSize2);
          char str78[100];
          char str81[100];
          dwret2 = dwret;
          _ultoa(dwret,str78,16);

          size_t len = strlen(str78)+1;
          size_t converted = 0;
          WCHAR *da = (WCHAR*)malloc( len*sizeof(WCHAR) );
          mbstowcs_s( &converted,da,len,str78,_TRUNCATE );
          QString str79 = QString::fromWCharArray( da );
          qDebug()<<str79<<str79.length();
          QString str80 = "0x";
          if( str79.length()<8 )
          {
              for( INT i = 0; i < (8-str79.length()); i++ )
              {
                  str80.append("0");
              }
          }
          str80.append(str79);
          str80.append("(");

          _ultoa(dwret2,str81,10);

          size_t len1 = strlen(str81)+1;
          size_t converted1 = 0;
          WCHAR *da1 = (WCHAR*)malloc( len1*sizeof(WCHAR) );
          mbstowcs_s( &converted1,da1,len1,str81,_TRUNCATE );
          QString str82 = QString::fromWCharArray( da1 );
          str80.append(str82);
          str80.append(")");

          QTableWidgetItem * item2 = new QTableWidgetItem(str80);
          ui->tableWidget->setItem(i,2,item2);

        }




        else if( ty == REG_EXPAND_SZ )
        {
          icon.addPixmap(QPixmap(":/521.PNG"));
          table->setIcon(icon);
          ui->tableWidget->setItem(i,0,table);

          QString ty1 = "REG_EXPAND_SZ";
          table1 = new QTableWidgetItem(ty1);
          ui->tableWidget->setItem(i,1,table1);

          QString str7 = QString::fromWCharArray( tcKeyvalue );
          //qDebug()<<tcKeyvalue<<str7;

          QTableWidgetItem *item2 = new QTableWidgetItem( str7 );
          ui->tableWidget->setItem( i,2,item2 );
        }




        else if( ty == REG_MULTI_SZ )
        {
          icon.addPixmap(QPixmap(":/521.PNG"));
          table->setIcon(icon);
          ui->tableWidget->setItem(i,0,table);

          QString ty1 = "REG_MULT_SZ";
          table1 = new QTableWidgetItem(ty1);
          ui->tableWidget->setItem(i,1,table1);

          QString str7 = QString::fromWCharArray( tcKeyvalue );
          //qDebug()<<tcKeyvalue<<str7;

          QTableWidgetItem *item2 = new QTableWidgetItem( str7 );
          ui->tableWidget->setItem( i,2,item2 );
         }




        else if( ty == REG_QWORD )
        {
          icon.addPixmap(QPixmap(":/5212.PNG"));
          table->setIcon(icon);
          ui->tableWidget->setItem(i,0,table);

          QString ty1 = "REG_QWORD";
          table1 = new QTableWidgetItem(ty1);
          ui->tableWidget->setItem(i,1,table1);

          QSettings * setting = new QSettings(str3,QSettings::NativeFormat);
          QString str7 = setting->value(str1).toString();


          QTableWidgetItem *item2 = new QTableWidgetItem( str7 );
          ui->tableWidget->setItem( i,2,item2 );
         }




        else if( ty == REG_BINARY )
        {
          icon.addPixmap(QPixmap(":/5212.PNG"));
          table->setIcon(icon);
          ui->tableWidget->setItem(i,0,table);

          QString ty1 = "REG_BINARY";
          table1 = new QTableWidgetItem(ty1);
          ui->tableWidget->setItem(i,1,table1);

          QString str7 = QString::fromWCharArray( tcKeyvalue );
          //qDebug()<<tcKeyvalue<<str7;

          QTableWidgetItem *item2 = new QTableWidgetItem( str7 );
          ui->tableWidget->setItem( i,2,item2 );


          BYTE valuea[1000] = { 0 };
          DWORD ty3;
          CHAR chvalue[100] = { 0 };
          DWORD dwKeyvalueSize4 = 1000;
          WCHAR tcKeyname4[128] = { 0 };
          DWORD dwKeyNameSize4 = 1000;
          RegEnumValue( hSsubKey, i,tcKeyname4, &dwKeyNameSize4, NULL, &ty3,
                        valuea, &dwKeyvalueSize4);
             INT ivalue;
             QString valuee;
          QByteArray temp((const char*)(valuea),dwKeyvalueSize4);
          for( INT i = 0; i < temp.count();i++)
          {
              ivalue = QVariant(temp[i]).toInt();
              if( 16 > ivalue && ivalue > -1)
              {
                  valuee.append("0");
              }
              itoa(ivalue,chvalue,16);
              QString strserial(chvalue);
              QString strl = strserial.right(2);

              valuee.append(strl);

              valuee.append(" ");
          qDebug()<<temp<<temp.count()<<strserial<<chvalue<<ivalue;
          }

          QTableWidgetItem * item3 = new QTableWidgetItem(valuee);
          ui->tableWidget->setItem(i,2,item3);

         }

        else if( ty == REG_DWORD_LITTLE_ENDIAN )
        {
          icon.addPixmap(QPixmap(":/521.PNG"));
          table->setIcon(icon);
          ui->tableWidget->setItem(i,0,table);

          QString ty1 = "REG_DWORD_LITTLE_ENDIAN";
          table1 = new QTableWidgetItem(ty1);
          ui->tableWidget->setItem(i,1,table1);

          QString str7 = QString::fromWCharArray( tcKeyvalue );
          //qDebug()<<tcKeyvalue<<str7;

          QTableWidgetItem *item2 = new QTableWidgetItem( str7 );
          ui->tableWidget->setItem( i,2,item2 );
         }

        else if( ty == REG_LINK )
        {
          icon.addPixmap(QPixmap(":/521.PNG"));
          table->setIcon(icon);
          ui->tableWidget->setItem(i,0,table);

          QString ty1 = "REG_LINK";
          table1 = new QTableWidgetItem(ty1);
          ui->tableWidget->setItem(i,1,table1);

          QString str7 = QString::fromWCharArray( tcKeyvalue );
          //qDebug()<<tcKeyvalue<<str7;

          QTableWidgetItem *item2 = new QTableWidgetItem( str7 );
          ui->tableWidget->setItem( i,2,item2 );
         }

        else if( ty == REG_FULL_RESOURCE_DESCRIPTOR )
        {
          icon.addPixmap(QPixmap(":/5212.PNG"));
          table->setIcon(icon);
          ui->tableWidget->setItem(i,0,table);

          QString ty1 = "REG_FULL_RESOURCE_DESCRIPTOR";
          table1 = new QTableWidgetItem(ty1);
          ui->tableWidget->setItem(i,1,table1);
          QString str7 = QString::fromWCharArray( tcKeyvalue );
          //qDebug()<<tcKeyvalue<<str7;

          QTableWidgetItem *item2 = new QTableWidgetItem( str7 );
          ui->tableWidget->setItem( i,2,item2 );

          BYTE valuea[1000] = { 0 };
          DWORD ty3;
          CHAR chvalue[100] = { 0 };
          DWORD dwKeyvalueSize4 = 1000;
          WCHAR tcKeyname4[128] = { 0 };
          DWORD dwKeyNameSize4 = 1000;
          RegEnumValue( hSsubKey, i,tcKeyname4, &dwKeyNameSize4, NULL, &ty3,
                        valuea, &dwKeyvalueSize4);
          INT ivalue;
          QString valuee;

          QByteArray temp((const char*)(valuea),dwKeyvalueSize4);
          for( INT i = 0; i < temp.count();i++)
          {
              ivalue = QVariant(temp[i]).toInt();
              if( 16 > ivalue &&
                  ivalue > -1)
              {
                  valuee.append("0");
              }
              itoa(ivalue,chvalue,16);
              QString strserial(chvalue);
              QString strl = strserial.right(2);

              valuee.append(strl);

              valuee.append(" ");
          qDebug()<<temp<<temp.count()<<strserial<<chvalue<<ivalue;
          }

          QTableWidgetItem * item3 = new QTableWidgetItem(valuee);
          ui->tableWidget->setItem(i,2,item3);

         }

        else if( ty == REG_RESOURCE_LIST )
        {
            icon.addPixmap(QPixmap(":/5212.PNG"));
            table->setIcon(icon);
            ui->tableWidget->setItem(i,0,table);

            QString ty1 = "REG_RESOURCE_LIST";
            table1 = new QTableWidgetItem(ty1);
            ui->tableWidget->setItem(i,1,table1);
            QString str7 = QString::fromWCharArray( tcKeyvalue );
            //qDebug()<<tcKeyvalue<<str7;

            QTableWidgetItem *item2 = new QTableWidgetItem( str7 );
            ui->tableWidget->setItem( i,2,item2 );
        }

        /*QString str7 = QString::fromWCharArray( tcKeyvalue );
        //qDebug()<<tcKeyvalue<<str7;

        QTableWidgetItem *item2 = new QTableWidgetItem( str7 );
        ui->tableWidget->setItem( i,2,item2 );*/
        dwKeyNameSize1 = 128;
     }
}


MainWindow::~MainWindow()
{
    delete ui;
}
