#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidgetItem>
#include <QTreeWidgetItem>
#include <windows.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void search( WCHAR*	ppath,QTreeWidgetItem* pfather,HKEY hMainKey);
    QTableWidgetItem *table;
    QTableWidgetItem *table1;
    QTreeWidgetItem *tree;
    QTreeWidgetItem *tree2;
    QTreeWidgetItem* prootchild;
    QTreeWidgetItem* userchild;
    QTreeWidgetItem* machinechild;
    QTreeWidgetItem* userschild;
    QTreeWidgetItem * proot;
    INT iob;
public slots:
    void type(QTreeWidgetItem* item,int n);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
