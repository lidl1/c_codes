#include <stdio.h>
#include <winsock2.h>
#include <stdlib.h>
#include <WS2tcpip.h>
#include <windows.h>
#include <iostream>

#pragma  comment( lib, "ws2_32.lib" )

#define SER_IP "127.0.0.1"
#define SERV_PORT 6666

int main()
{
	WSADATA ws;
	WSAStartup( MAKEWORD( 2, 2 ), &ws );

	SOCKET clientSocket;
	clientSocket = socket( AF_INET, SOCK_STREAM, 0 );
	if( SOCKET_ERROR == clientSocket )
	{
		printf("create socket fail\n");
	}

	sockaddr_in socketAddr;
	socketAddr.sin_family = AF_INET;
	socketAddr.sin_addr.s_addr = inet_addr( SER_IP );
	socketAddr.sin_port = htons( SERV_PORT );

	int iRes = connect( clientSocket, (SOCKADDR*)&socketAddr, sizeof(SOCKADDR) );
	if( SOCKET_ERROR == iRes )
	{
		printf("connect error\n");
	}

	while( 1 )
	{
		char c[1024] = {};
		std::cin>>c;

		send( clientSocket, c, strlen(c), 0 );

		char cr[1024] = {};
		recv( clientSocket, cr, 1024, 0 );

		std::cout<<cr<<std::endl;

	}

	closesocket( clientSocket );

	WSACleanup();

	return 0;
}