#include <stdio.h>
#include <winsock2.h>
#include <stdlib.h>
#include <WS2tcpip.h>
#include <windows.h>
#include <iostream>

#define SER_IP "127.0.0.1"
#define SERV_PORT 6666

int main()
{
	SOCKET lfd, cfd;
	sockaddr_in serv_addr,clie_addr;
	int ret = 0;
	socklen_t clie_addr_len;
	char buf[BUFSIZ];
	
	WSADATA wd;
	WSAStartup( MAKEWORD( 2, 2), &wd );

	lfd = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );
	if( SOCKET_ERROR == lfd )
	{
		printf("socket error!\n");
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons( SERV_PORT );
	serv_addr.sin_addr.s_addr = htonl( INADDR_ANY );

	
	ret = bind( lfd,( struct sockaddr* )&serv_addr, sizeof( serv_addr ) );
	if( SOCKET_ERROR == ret )
	{
		printf("bind error\n");
	}

	ret = listen( lfd, SOMAXCONN );
	if( SOCKET_ERROR == ret )
	{
		printf("listen error\n");
	}

	clie_addr_len = sizeof( clie_addr );

	cfd = accept( lfd, (sockaddr *)&clie_addr, &clie_addr_len );
	if( SOCKET_ERROR == cfd )
	{
		printf("accept error\n");
	}

	while( 1 )
	{
		//Sleep( 5 *1000 );
		//ZeroMemory( buf, 1024 );
		ret = recv( cfd, buf, sizeof( buf ), 0 );
		printf("%s\n",buf );

		send( cfd, buf, sizeof( buf ), 0 );
	}

	closesocket( lfd );
	closesocket( cfd );

	WSACleanup();


	return 0;


}