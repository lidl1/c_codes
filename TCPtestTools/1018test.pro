#-------------------------------------------------
#
# Project created by QtCreator 2021-10-18T15:53:00
#
#-------------------------------------------------

QT       += core gui
QT       += network
UI_DIR = ./
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 1018test
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    cthread.cpp

HEADERS  += widget.h \
    cthread.h

FORMS    += widget.ui

RESOURCES += \
    res.qrc
