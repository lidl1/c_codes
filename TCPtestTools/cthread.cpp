#include <cthread.h>
#include <widget.h>
#include <QTcpSocket>
#include <QIcon>


CThread::CThread(
        CThread *   parent
        )
{


    //connect( pSocket,     &QTcpSocket::disconnected, this, &CWidget::socket_disconnect );
    QTcpSocket* pSocket = NULL;
    QListWidget* pListWidget = NULL;
    QListWidget* pListWidget2 = NULL;
    QCheckBox* pCheckBox = NULL;

    QLabel* plabelSend = NULL;
    QLabel* plabelRec = NULL;
    QLabel* plabelSuc = NULL;
    QLabel* plabelErr = NULL;

    int* piSend = NULL;
    int* piRec = NULL;
    int* piSuc = NULL;
    int* pierr = NULL;
}

void
CThread::run(
        void
        )
{
    mutex.lock();

    pSocket = new  QTcpSocket();
    *piSend = *piSend + 1;
    //单加不好使，用waitForReadyRead函数代替
    //connect( pSocket,     &QTcpSocket::readyRead,  this, &CThread::socket_read );

    pSocket->abort();
    pSocket->connectToHost( strIP, iPort );

    //连接超时
    if( !pSocket->waitForConnected( 10000 ) )
    {
        qDebug()<<"connected failed"<<endl;    

        *pierr = *pierr + 1;

        //设置失败图标
        QIcon icon( QPixmap( ":/false.PNG" ) );
        setItem( icon );

        showNums();

        return;
    }

    //连接成功
    qDebug()<<"connect successfully"<<endl;
    *piSuc = *piSuc + 1;

    //设置成功图标
    QIcon icon( QPixmap( ":/true.PNG" ) );
    setItem( icon );
    //发送第一次数据
    pSocket->write( btArray );
    pSocket->flush();

    //设置槽函数，有问题
    //connect( pListWidget2, SIGNAL( itemDoubleClicked(QListWidgetItem*) ),this,SLOT( showMes(QListWidgetItem*) ) );

    //等待接收数据
    if( pSocket->waitForReadyRead() )
    {
        socket_read();
        *piRec = *piRec + 1;
    }

    //检查是否发送第二次信息
    if( pCheckBox->isChecked() )
    {
        //发送第二次数据
        pSocket->write( btArraySec );
        pSocket->flush();
    }

    showNums();

    //断开连接
    pSocket->disconnectFromHost();
    mutex.unlock();

}

void
CThread::socket_read(
        void
        )
{
    QByteArray btBuffer;
    //读取返回数据
    btBuffer = pSocket->readAll();
    qDebug()<<btBuffer.toHex()<<endl;

    //将16进制数据转换
    QString str = tr( btBuffer.toHex() );

    pListWidget->addItem( new QListWidgetItem( str ) );

}

void
CThread::setItem(
        QIcon       icon
        )
{

    //获取当前线程号
    int iThread = (int)QThread::currentThreadId();
    QString str = QString::number( iThread );

    //获取线程启动号
    QString strStart = QString::number( iNum );
    QListWidgetItem* pListWidgetItem = new QListWidgetItem( strStart+ " 线程号："+str );

    //设置ListWidgetItem
    pListWidgetItem->setIcon( icon );
    pListWidget2->addItem( pListWidgetItem );

}

void
CThread::showNums(
        void
        )
{
    //qDebug()<<*piRec<<" "<<*piSuc<<" "<<*pierr<<endl;
    plabelSend->setText( QString::number(*piSend) );
    plabelRec->setText( QString::number(*piRec) );
    plabelSuc->setText( QString::number(*piSuc) );
    plabelErr->setText( QString::number(*pierr) );

}

void
CThread::showMes(
        QListWidgetItem* p
        )
{
    qDebug()<<p->text()<<" "<<p->whatsThis()<<endl;
}

CThread::~CThread(
        void
        )
{
    delete this->pSocket;
}
