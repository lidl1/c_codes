#ifndef CTHREAD_H
#define CTHREAD_H
#include <QThread>
#include <QByteArray>
#include <QTcpSocket>
#include <QListWidget>
#include <QCheckBox>
#include <QLabel>
#include <QMutex>
#include <QMutexLocker>




class CThread : public QThread
{
    Q_OBJECT

public:
    explicit CThread(CThread *parent = 0);
    ~CThread();

    QString strIP;
    QTcpSocket* pSocket;
    QByteArray btArray;
    QByteArray btArraySec;
    QListWidget* pListWidget;
    QListWidget* pListWidget2;
    QCheckBox* pCheckBox;

    QLabel* plabelSend;
    QLabel* plabelRec;
    QLabel* plabelSuc;
    QLabel* plabelErr;

    int* piSend;
    int* piRec;
    int* piSuc;
    int* pierr;

    int iPort;
    int iNum;
    void run();
    void setItem( QIcon icon );
    void showNums();

    QMutex mutex;

public slots:
    void showMes( QListWidgetItem* p );

public slots:
    void socket_read();

};

#endif // CTHREAD_H
