#include "widget.h"
#include "ui_widget.h"
#include <qdebug.h>
#include <cthread.h>
#include <QMutex>

int g_iSend = 0;
int g_iRec = 0;
int g_iSuc = 0;
int g_ierr = 0;

CWidget::CWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    connect( ui->pushButton_3, &QPushButton::clicked,     this, &CWidget::startThread );


    //设置初始值
    ui->lineEdit_IP->setText( "192.168.0.91" );
    ui->lineEdit_Port->setText("22100");
    ui->lineEdit->setText( "1" );
    ui->textEdit_send->setText( "0100000000000000000000000000000032bef8b6674d8b0388f6c23d0ef1243f3e2dc15333248762e48ce9aaf3057a7100000000000000000000000000000000" );
    ui->textEdit_recv->setText( "01000000" );
    ui->checkBox->setChecked( true );

}


void
CWidget::startThread(
        void
        )
{
    //清屏
    ui->listWidget->clear();
    ui->listWidget_2->clear();

    //开始执行
    if( ui->pushButton_3->text() == tr("开始测试") )
    {
        ui->pushButton_3->setText( "停止测试" );
        int n = ui->lineEdit->text().toInt();

        QString strIP = ui->lineEdit_IP->text();
        int iPort = ui->lineEdit_Port->text().toInt();

        //将十六进制数据转换为二进制发送
        QByteArray btArray = QByteArray::fromHex( ui->textEdit_send->toPlainText().toLocal8Bit() );
        QByteArray btArray2 = QByteArray::fromHex( ui->textEdit_recv->toPlainText().toLocal8Bit() );

        qDebug()<<"first"<<ui->textEdit_send->toPlainText()<<endl;
        qDebug()<<"second"<<ui->textEdit_recv->toPlainText()<<endl;

        for( int i = 0; i < n; i++ )
        {
            CThread* pThread = new CThread();

            pThread->iNum = i;
            pThread->strIP = strIP;
            pThread->iPort = iPort;
            pThread->btArray = btArray;
            pThread->btArraySec = btArray2;
            pThread->pListWidget2 = ui->listWidget;
            pThread->pListWidget = ui->listWidget_2;
            pThread->pCheckBox = ui->checkBox;

            pThread->plabelSend = ui->label_5;
            pThread->plabelRec = ui->label_7;
            pThread->plabelSuc = ui->label_10;
            pThread->plabelErr = ui->label_11;

            pThread->piSend = &g_iSend;
            pThread->piRec = &g_iRec;
            pThread->piSuc = &g_iSuc;
            pThread->pierr = &g_ierr;

            pThread->start();
        }

    }

    //恢复按钮状态
    ui->pushButton_3->setText( "开始测试" );
}

CWidget::~CWidget()
{
    delete ui;
}
