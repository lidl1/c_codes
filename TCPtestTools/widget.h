#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <windows.h>
#include <QString>
#include <QTextEdit>
#include <QTcpSocket>
//#include <winsock2.h>
//#include <winsock.h>
#include <iostream>
#include <QThread>

namespace Ui {
class Widget;
}

class CWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CWidget(QWidget *parent = 0);
    ~CWidget();

    void CheckSendTimes(  );
    void on_push_connect_clicked();
    QTextEdit*      pTextEdit;

private:
    Ui::Widget *ui;


public slots:
    void startThread();

};

#endif // WIDGET_H
