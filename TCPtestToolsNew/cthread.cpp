#include "cthread.h"
#include <QRunnable>
#include <QThread>
#include <QDebug>

Runnable::Runnable():QRunnable()
{
}

Runnable::~Runnable(
        void
        )
{
}

void
Runnable::run(
        void
        )
{

    pSocket = new  QTcpSocket();

    pSocket->abort();
    pSocket->connectToHost( strIP, iPort );

    //连接超时
    if( !pSocket->waitForConnected( 5000 ) )
    {
        //qDebug()<<"connected failed"<<endl;
        return;
    }

    //连接成功
    //qDebug()<<"connect successfully"<<endl;


    //发送第一次数据
    pSocket->write( btArray );
    pSocket->flush();

    //等待接收数据
    if( pSocket->waitForReadyRead() )
    {
        //QByteArray btBuffer;
        //读取返回数据
        //btBuffer = pSocket->readAll();
        //qDebug()<<pSocket->readAll().toHex()<<endl;

    }

    //发送第二次数据
    pSocket->write( btArraySec );
    pSocket->flush();

    //断开连接
    pSocket->disconnectFromHost();

    delete( pSocket );
}
