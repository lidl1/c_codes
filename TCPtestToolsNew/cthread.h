#ifndef CTHREAD_H
#define CTHREAD_H

#include <QRunnable>
#include <QTcpSocket>
#include <QListWidget>
#include <QCheckBox>
#include <QLabel>
#include <QMutex>
#include <QMutexLocker>

class Runnable :public QRunnable
{

public:
    Runnable();
    ~Runnable();

    QString strIP;
    QTcpSocket* pSocket;
    QByteArray btArray;
    QByteArray btArraySec;

    int iPort;
    void run();
};


#endif // CTHREAD_H
