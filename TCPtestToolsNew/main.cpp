#include "widget.h"
#include <QApplication>
#include <QThreadPool>
#include "cthread.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    w.show();

    return a.exec();
}
