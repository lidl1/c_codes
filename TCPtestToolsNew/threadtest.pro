#-------------------------------------------------
#
# Project created by QtCreator 2021-10-26T13:03:15
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = threadtest
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    cthread.cpp

HEADERS  += widget.h \
    cthread.h

FORMS    += widget.ui
