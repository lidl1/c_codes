#include "widget.h"
#include "ui_widget.h"
#include "cthread.h"
#include <QThreadPool>


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    connect( ui->pushButton, &QPushButton::clicked, this, &Widget::startThread );

    //设置端口、ip、线程数
    ui->lineEdit->setText( "10.39.87.101" );
    ui->lineEdit_2->setText("22100");
    ui->lineEdit_3->setText( "10000" );

    //设置默认发送数据
    ui->textEdit->setText( "0100000000000000000000000000000032bef8b6674d8b0388f6c23d0ef124"
                           "3f3e2dc15333248762e48ce9aaf3057a7100000000000000000000000000000000" );
    ui->textEdit_2->setText( "01000000" );

    //设置是否发送第二次
    ui->checkBox->setChecked( true );

    setWindowTitle( "Tcp" );
}

void
Widget::startThread(
        void
        )
{
    int iNum = 0;

    if( ui->pushButton->text() == tr("开始测试") )
    {
        ui->pushButton->setText( "停止测试" );

        //获取线程数和端口号
        int n = ui->lineEdit_3->text().toInt();
        int iPort = ui->lineEdit_2->text().toInt();

        //获取ip
        QString strIP = ui->lineEdit->text();

        //将十六进制数据转换为二进制发送
        QByteArray btArray = QByteArray::fromHex( ui->textEdit->toPlainText().toLocal8Bit() );
        QByteArray btArray2 = QByteArray::fromHex( ui->textEdit_2->toPlainText().toLocal8Bit() );

        //qDebug()<<"first"<<ui->textEdit->toPlainText()<<endl;
        //qDebug()<<"second"<<ui->textEdit_2->toPlainText()<<endl;

        //设置线程池
        QThreadPool * pThreadPool = QThreadPool::globalInstance();
        if( pThreadPool )
        {
            //设置线程池默认属性
            pThreadPool->setMaxThreadCount( 700 );
            pThreadPool->setExpiryTimeout( 1000 );           

            //启动线程
            for( int i = 0; i < n; i++ )
            {
                Runnable* pThread = new Runnable();

                //设置线程属性
                pThread->strIP = strIP;
                pThread->iPort = iPort;
                pThread->btArray = btArray;
                pThread->btArraySec = btArray2;

                //将线程塞进线程池
                pThreadPool->start( pThread );

                iNum++;
            }
        }
     }

    qDebug()<<iNum<<endl;

    //恢复按钮状态
    ui->pushButton->setText( "开始测试" );
}

Widget::~Widget()
{
    delete ui;
}
