#include "LogEvent.h"

CLogEvent::CLogEvent()
{

}

CLogEvent::~CLogEvent()
{

}

BOOL
CLogEvent::LogWriter(
	IN DWORD			dwID,
	IN WORD				wLogEventType,
	IN WORD				wCateGory,
	IN LPCTSTR			pszFormat,
		...
	)
{
	BOOL bStatus = FALSE;

	do
	{
		if( NULL == pszFormat )
		{
			cout << "pszFormat = NULL" << endl;
			break;
		}

		TCHAR	tchBuf[ 1024 ] = { 0 };
		va_list arglist = NULL;

		va_start( arglist, pszFormat );
		_vstprintf_s( tchBuf, 1023, pszFormat, arglist );
		va_end( arglist );

		LPCSTR pBuf = tchBuf;
		if( NULL == pBuf )
		{
			cout << "pBuf = NULL" << endl;
			break;
		}

		HANDLE	hEventLog = RegisterEventSource( NULL, _T("test") );
		if( NULL == hEventLog )
		{
			cout << "hEventLog = NULL" << endl;
			break;
		}
		
		bStatus = ReportEvent( hEventLog,
							   wLogEventType,
							   wCateGory,
							   dwID,
							   NULL,
							   1,
							   0,
							   &pBuf,
							   NULL
							  );
		if( !bStatus )
		{
			cout << "ReportEvent failed" << endl;
			break;
		}
		
		bStatus = DeregisterEventSource( hEventLog );
		if( !bStatus )
		{
			cout << "DeregisterEventSource failed" << endl;
			break;
		}
	}while( FALSE );

	if( bStatus )
	{
		cout << "success insert log" << endl;
	}

	return bStatus;
}

BOOL 
CLogEvent::LogWriter( 
	IN DWORD			dwID,
	IN WORD				wLogEventType,
	IN WORD				wCateGory
	)
{
	BOOL bStatus = FALSE;

	do
	{
		HANDLE	hEventLog = RegisterEventSource( NULL, _T("test") );
		if( NULL == hEventLog )
		{
			cout << "hEventLog = NULL" << endl;
			break;
		}

		bStatus = ReportEvent( hEventLog,
							   wLogEventType,
							   wCateGory,
							   dwID,
							   NULL,
							   0,
							   0,
							   NULL,
							   NULL
							  );
		if( !bStatus )
		{
			cout << "ReportEvent failed" << endl;
			break;
		}

		bStatus = DeregisterEventSource( hEventLog );
		if( !bStatus )
		{
			cout << "DeregisterEventSource failed" << endl;
			break;
		}
	}while( FALSE );

	if( bStatus )
	{
		cout << "success insert log" << endl;
	}

	return bStatus;
	
}

BOOL 
CLogEvent::LogWriter(
	IN DWORD			dwID,
	IN WORD				wLogEventType,
	IN WORD				wCateGory,
	IN WORD				dwCount,
	IN LPCTSTR			pszFormat,
		...
		)
{
	BOOL bStatus = FALSE;

	do
	{
		if( NULL == pszFormat )
		{
			cout << "pszFormat = NULL" << endl;
			break;
		}

		va_list arglist = NULL;
		LPCTSTR pBuf[ 64 ] = { 0 };

		va_start( arglist, pszFormat );
		pBuf[0] = pszFormat;

		for( INT i = 1; i < dwCount; i++ )
		{
			pBuf[i] = va_arg( arglist, LPCTSTR );
		}

		va_end( arglist );

		HANDLE	hEventLog = RegisterEventSource( NULL, _T("test") );
		if( NULL == hEventLog )
		{
			cout << "hEventLog = NULL" << endl;
			break;
		}

		bStatus = ReportEvent( hEventLog,
							   wLogEventType,
							   wCateGory,
							   dwID,
							   NULL,
							   dwCount,
							   0,
							   pBuf,
							   NULL
							  );
		if( !bStatus )
		{
			cout << "ReportEvent failed" << endl;
			break;
		}

		bStatus = DeregisterEventSource( hEventLog );
		if( !bStatus )
		{
			cout << "DeregisterEventSource failed" << endl;
			break;
		}
	} while( FALSE );

	if( bStatus )
	{
		cout << "success insert log" << endl;
	}

	return bStatus;
}


BOOL 
CLogEvent::AddLogSource( 
	IN LPCTSTR				pLogSourceName
	)
{
	BOOL bStatus = FALSE;

	do 
	{
		if( NULL == pLogSourceName )
		{
			cout << "pLogSourceName = NULL" << endl;
			break;
		}

		HKEY	hRegKey = NULL;
		TCHAR	szPath[ MAX_PATH ] = { 0 };

		_sntprintf_s( szPath, MAX_PATH - 1, TEXT("SYSTEM\\CurrentControlSet\\Services\\EventLog\\%s"), pLogSourceName );

		bStatus = OpenKey( szPath, &hRegKey );
		if( !bStatus )
		{
			cout << "open key failed" << endl;
			break;
		}

		bStatus = SetLogSourceValue( &hRegKey );
		if( !bStatus )
		{
			cout << "SetValue failed" << endl;
			break;
		}

		bStatus = TRUE;
		cout << "set value success" << endl;

		RegCloseKey( hRegKey );

	} while( FALSE );

	return bStatus;
	
}

BOOL 
CLogEvent::AddEventSource(	
	IN LPCTSTR				pLogSourceName,
	IN LPCTSTR				pEventSourceName
	)
{
	BOOL bStatus = FALSE;

	do 
	{
		if( NULL == pEventSourceName )
		{
			cout << "pEventSourceName = NULL" << endl;
			break;
		}

		HKEY		hRegKey = NULL;
		TCHAR		szPath[ MAX_PATH ] = { 0 };		
		
		_sntprintf_s( szPath, MAX_PATH - 1, TEXT("SYSTEM\\CurrentControlSet\\Services\\EventLog\\%s\\%s"), pLogSourceName, pEventSourceName );
		
		bStatus = OpenKey( szPath, &hRegKey );
		if( !bStatus )
		{
			cout << "open key failed" << endl;
			break;
		}

		bStatus = SetEventSourceValue( &hRegKey, 3 );
		if( !bStatus )
		{
			cout << "SetValue failed" << endl;
			break;
		}
			
		bStatus = TRUE;
		cout << "set value success" << endl;

		RegCloseKey( hRegKey );

	}while( FALSE );
	
	return bStatus;
		
}
		
BOOL
CLogEvent::OpenKey( 
	IN LPCTSTR			pPath,
	OUT PHKEY			pHkey
	)
{
	BOOL bStatus = FALSE;

	do 
	{
		if( (NULL == pPath) ||
			(NULL == pHkey) )
		{
			cout << "pPath = NULL or PHkey = NULL" << endl;
			break;
		}
			
		DWORD		dwError = 0;

		if( ERROR_SUCCESS != RegCreateKeyEx( HKEY_LOCAL_MACHINE, pPath, 0, NULL, REG_OPTION_NON_VOLATILE , KEY_ALL_ACCESS, NULL, pHkey, &dwError ) )
		{
			cout << "open key failed" << endl;
			break;
		}

		if( REG_CREATED_NEW_KEY == dwError )
		{
			cout << "该键之前不存在，新建的" << endl;
		}
		else if( REG_OPENED_EXISTING_KEY == dwError )
		{
			cout << "该键已存在" << endl;
		}

		bStatus = TRUE;

	}while( FALSE );
	
	return bStatus;
}




BOOL
CLogEvent::SetEventSourceValue( 
	IN PHKEY			pHkey,
	IN DWORD			dwCateGoryCount
	)
{
	BOOL bStatus = FALSE;

	do 
	{
		if( NULL == pHkey )
		{
			cout << "PHkey = NULL" << endl;
			break;
		}

		TCHAR tchPath[ MAX_PATH ] = TEXT("d:\\mes.dll");
		long lReturn = RegSetValueEx( *pHkey, _T("EventMessageFile"), 0, REG_EXPAND_SZ, ( PBYTE )tchPath, ( _tcslen(tchPath) + 1 ) * sizeof( TCHAR ) );
		if( ERROR_SUCCESS != lReturn )
		{
			cout << "set value failed" << endl;
			break;
		}

		lReturn = RegSetValueEx( *pHkey, _T("ParameterMessageFile"), 0, REG_EXPAND_SZ, ( PBYTE )tchPath, ( _tcslen(tchPath) + 1 ) * sizeof( TCHAR ) );
		if( ERROR_SUCCESS != lReturn )
		{
			cout << "set value failed" << endl;
			break;
		}
		
		DWORD dwTypes = EVENTLOG_ERROR_TYPE | EVENTLOG_WARNING_TYPE | EVENTLOG_INFORMATION_TYPE;

		lReturn = RegSetValueEx( *pHkey, _T("TypesSupported"), 0, REG_DWORD, ( LPBYTE )&dwTypes, sizeof( dwTypes ) );
		if( ERROR_SUCCESS != lReturn )
		{
			cout << "set value2 failed" << endl;
			break;
		}

		lReturn = RegSetValueEx( *pHkey, _T("CategoryMessageFile"), 0, REG_EXPAND_SZ, ( PBYTE )tchPath, ( _tcslen(tchPath) + 1 ) * sizeof(TCHAR) );
		if( ERROR_SUCCESS != lReturn )
		{
			cout << "set value3 failed" << endl;
			break;
		}

		lReturn = RegSetValueEx( *pHkey, _T("CategoryCount"), 0, REG_DWORD, ( PBYTE )&dwCateGoryCount, sizeof( dwCateGoryCount ) );
		if( ERROR_SUCCESS != lReturn )
		{
			cout << "set value4 failed" << endl;
			break;
		}

		bStatus = TRUE;

	}while( FALSE );
	
	return bStatus;
}


BOOL 
CLogEvent::SetLogSourceValue( 
	IN PHKEY				pHkey
	)
{
	BOOL bStatus = FALSE;

	do 
	{
		if( NULL == pHkey )
		{
			cout << "pHkey = NULL!" << endl;
		}

		TCHAR tchPath[ MAX_PATH ] = TEXT("%systemroot%\\system32\\winevt\\logs\\ldltest.evtx");//日志存放路径
		long lReturn = RegSetValueEx( *pHkey, _T("File"), 0, REG_EXPAND_SZ, ( PBYTE )tchPath, ( _tcslen(tchPath) + 1 ) * sizeof( TCHAR ) );
		if( ERROR_SUCCESS != lReturn )
		{
			cout << "set value failed" << endl;
			break;
		}

		DWORD dwMaxSize = 20971520;//20MB
		lReturn = RegSetValueEx( *pHkey, _T("MaxSize"), 0, REG_DWORD, ( PBYTE )&dwMaxSize, sizeof( dwMaxSize ) );
		if( ERROR_SUCCESS != lReturn )
		{
			cout << "set value failed2" << endl;
			break;
		}

		ZeroMemory( tchPath, MAX_PATH );
		_tcsncpy_s( tchPath, MAX_PATH, TEXT("ldltest"), _TRUNCATE );
		lReturn = RegSetValueEx( *pHkey, _T("PrimaryModule"), 0, REG_SZ, ( PBYTE )tchPath, ( _tcslen(tchPath) + 1 ) * sizeof( TCHAR ) );//日志源名称
		if( ERROR_SUCCESS != lReturn )
		{
			cout << "set value failed3" << endl;
			break;
		}

		DWORD dwSave = 0;//始终覆盖日志
		lReturn = RegSetValueEx( *pHkey, _T("Retention"), 0, REG_DWORD, ( PBYTE )&dwSave, sizeof( dwSave ) );
		if( ERROR_SUCCESS != lReturn )
		{
			cout << "set value failed4" << endl;
			break;
		}

		ZeroMemory( tchPath, MAX_PATH );
		_tcsncpy_s( tchPath, MAX_PATH, TEXT("test"), _TRUNCATE );//向日志写入事件的事件源名称
		lReturn = RegSetValueEx( *pHkey, _T("Sources"), 0, REG_MULTI_SZ, ( PBYTE )tchPath, ( _tcslen(tchPath) + 1 ) * sizeof( TCHAR ) );
		if( ERROR_SUCCESS != lReturn )
		{
			cout << "set value failed5" << endl;
			break;
		}

		DWORD dwAutoBackup = 0;//始终覆盖日志
		lReturn = RegSetValueEx( *pHkey, _T("AutoBackupLogFiles"), 0, REG_DWORD, ( PBYTE )&dwAutoBackup, sizeof( dwAutoBackup ) );
		if( ERROR_SUCCESS != lReturn )
		{
			cout << "set value failed6" << endl;
			break;
		}

		DWORD dwStrict = 1;//限制来宾和匿名用户访问
		lReturn = RegSetValueEx( *pHkey, _T("RestrictGuestAccess"), 0, REG_DWORD, ( PBYTE )&dwStrict, sizeof( dwStrict ) );
		if( ERROR_SUCCESS != lReturn )
		{
			cout << "set value failed7" << endl;
			break;
		}
		//剩一个Isolation为定义默认访问权限 默认为应用程序

		bStatus = TRUE;

	}while( FALSE );

	return bStatus;
}


BOOL 
CLogEvent::CopyEventfile( 
	IN LPCTSTR				pFileName,
	IN LPCTSTR				pTarget 
	)
{
	BOOL bStatus = TRUE;

	TCHAR tchCmd[ 1024 ] = { 0 };

	_sntprintf_s( tchCmd, MAX_PATH - 1, TEXT("copy C:\\Windows\\System32\\winevt\\Logs\\%s.evtx %s"), pFileName, pTarget );

	if( Wow64EnableWow64FsRedirection(FALSE) )
	{
		system(tchCmd);

		Wow64EnableWow64FsRedirection( TRUE );
	}
	else
	{
		bStatus = FALSE;
	}

	return bStatus;
}