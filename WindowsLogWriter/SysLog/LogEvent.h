#include "pch.h"

class CLogEvent
{
public:

	CLogEvent();
	~CLogEvent();

public:

	static BOOL
	LogWriter(
		IN DWORD			dwID,
		IN WORD				wLogEventType,
		IN WORD				wCateGory,
		IN LPCTSTR			pszFormat,
			...
		);

	static BOOL
	LogWriter(
		IN DWORD			dwID,
		IN WORD				wLogEventType,
		IN WORD				wCateGory
		);

	static BOOL
	LogWriter(
		IN DWORD			dwID,
		IN WORD				wLogEventType,
		IN WORD				wCateGory,
		IN WORD				dwCount,
		IN LPCTSTR			pszFormat,
			...
	);

	static BOOL
	AddLogSource(
		IN LPCTSTR			pLogSourceName
	);

	static BOOL 
	AddEventSource(
		IN LPCTSTR			pLogSourceName,
		IN LPCTSTR			pEventSourceName		
		);

	static BOOL 
	CopyEventfile(
		IN LPCTSTR			pFileName,
		IN LPCTSTR			pTarget	
	);
	
private:

	static BOOL 
	OpenKey(
		IN LPCTSTR			pPath,
		OUT PHKEY			pHkey
	);

	static BOOL
	SetEventSourceValue(
		IN PHKEY			pHkey,
		IN DWORD			dwCateGaryCount
	);

	static BOOL
	SetLogSourceValue(
		IN PHKEY			pHkey
	);
};
