 // ***** TestEventLog.mc *****
 // This is the header.
//
//  Values are 32 bit values layed out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//
#define FACILITY_SYSTEM                  0x0
#define FACILITY_STUBS                   0x3
#define FACILITY_RUNTIME                 0x2
#define FACILITY_IO_ERROR_CODE           0x4


//
// Define the severity codes
//
#define STATUS_SEVERITY_WARNING          0x2
#define STATUS_SEVERITY_SUCCESS          0x0
#define STATUS_SEVERITY_INFORMATIONAL    0x1
#define STATUS_SEVERITY_ERROR            0x3


//
// MessageId: TYPE_1
//
// MessageText:
//
//  表现好
//
#define TYPE_1                           ((DWORD)0x00000001L)

//
// MessageId: TYPE_2
//
// MessageText:
//
//  表现不好
//
#define TYPE_2                           ((DWORD)0x00000002L)

//
// MessageId: TYPE_3
//
// MessageText:
//
//  表现一般
//
#define TYPE_3                           ((DWORD)0x00000003L)

 // The following are message definitions.
//
// MessageId: MSG_START
//
// MessageText:
//
//  上班了%1
//
#define MSG_START                        ((DWORD)0x400203E9L)

//
// MessageId: MSG_STOP
//
// MessageText:
//
//  下班了%1
//
#define MSG_STOP                         ((DWORD)0x400203EAL)

//
// MessageId: MSG_ING
//
// MessageText:
//
//  工作时间
//
#define MSG_ING                          ((DWORD)0x400203EBL)

//
// MessageId: MSG_LDL
//
// MessageText:
//
//  %1啊啊啊%2啊啊%3
//
#define MSG_LDL                          ((DWORD)0x400203ECL)

