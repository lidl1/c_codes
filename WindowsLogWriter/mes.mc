; // ***** TestEventLog.mc *****

; // This is the header.

MessageIdTypedef=DWORD

SeverityNames=(	Success=0x0:STATUS_SEVERITY_SUCCESS
				Informational=0x1:STATUS_SEVERITY_INFORMATIONAL
				Warning=0x2:STATUS_SEVERITY_WARNING
				Error=0x3:STATUS_SEVERITY_ERROR
				)


FacilityNames=(	System=0x0:FACILITY_SYSTEM
				Runtime=0x2:FACILITY_RUNTIME
				Stubs=0x3:FACILITY_STUBS
				Io=0x4:FACILITY_IO_ERROR_CODE
				)


LanguageNames=(Chinese=0x804:MSG00804)

MessageId = 0x1
SymbolicName = TYPE_1
Language = Chinese
表现好
.

MessageId = 0x2
SymbolicName = TYPE_2
Language = Chinese
表现不好
.

MessageId = 0x3
SymbolicName = TYPE_3
Language = Chinese
表现一般
.

; // The following are message definitions.

MessageId=1001
Severity=Informational
Facility=Runtime
SymbolicName=MSG_START
Language=Chinese
上班了%1
.
MessageId=1002
Severity=Informational
Facility=Runtime
SymbolicName=MSG_STOP
Language=Chinese
下班了%1
.
MessageId=1003
Severity=Informational
Facility=Runtime
SymbolicName=MSG_ING
Language=Chinese
工作时间
.
MessageId=1004
Severity=Informational
Facility=Runtime
SymbolicName=MSG_LDL
Language=Chinese
%1啊啊啊%2啊啊%3
.

