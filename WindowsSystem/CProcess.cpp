#include "CProcess.h"

CProcess::CProcess(
	VOID
	)
{
}

CProcess::~CProcess(
	VOID
	)
{
}


BOOL
CProcess::CreateProc(
	IN string  strProcName
	)
{
	BOOL bStatus = FALSE;

	do
	{
		if( 0 == strProcName.size() )
		{
			break;
		}

		CString strName( strProcName.c_str() );

		STARTUPINFO si = { sizeof( si ) };
		PROCESS_INFORMATION pi = { 0 };

		si.cb = sizeof(STARTUPINFO);
		si.dwFlags = STARTF_USESHOWWINDOW;
		si.wShowWindow = SW_SHOW;

		bStatus = CreateProcess( strName, NULL, NULL, NULL, 
								 FALSE, 0, NULL, NULL, &si, &pi );
		if( !bStatus )
		{
			printf("create proc error\n");
			break;
		}

		printf("suc\n");

	}while( FALSE );

	return bStatus;
}


HANDLE
CProcess::GetProcHandle(
	IN INT iProc
)
{
	if( 0 > iProc )
	{
		return NULL;
	}

	return OpenProcess( PROCESS_ALL_ACCESS, TRUE, iProc );
}


BOOL
CProcess::ShowProcList(
	VOID
	)
{
	BOOL bStatus = FALSE;

	do
	{
		HANDLE hProcessSnap = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
		PROCESSENTRY32 process = { sizeof( PROCESSENTRY32 ) };

		INT i = 0;

		while( Process32Next( hProcessSnap, &process ) )
		{
			i++;
			cout << process.szExeFile << ends << process.th32ProcessID << endl;

			//CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS || PROCESS_VM_READ || PROCESS_QUERY_INFORMATION, process.th32ProcessID );

			CHAR cProcPath[MAX_PATH] = { 0 };
			GetProcessImageFileName( GetProcHandle( process.th32ProcessID ), cProcPath, MAX_PATH );

			cout << cProcPath << endl;
		}
		cout << i;
	
	}while( FALSE );

	return bStatus;
}

BOOL
CProcess::KillProcess( 
	IN INT iProc
	)
{
	BOOL bStatus = FALSE;

	do
	{
		HANDLE hProcess = NULL;

		hProcess = GetProcHandle( iProc );
		if( NULL == hProcess )
		{
			printf("no such proc\n");
			break;
		}

		bStatus = TerminateProcess( hProcess, -1 );
		if( !bStatus )
		{
			printf("kill proc error\n");
			break;
		}

	} while( FALSE );
	
	return bStatus;
}

