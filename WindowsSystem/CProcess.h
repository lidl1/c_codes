#include "stdafx.h"

class CProcess{

	CProcess();
	~CProcess();

public:

static BOOL
CreateProc(
	IN	string	strProcName
	);

static BOOL 
KillProcess(
	IN INT		iProc
	);

static BOOL 
KillProcess(
	IN string	strProcName
	);

static BOOL 
ShowProcList(
	VOID
	);

static HANDLE
GetProcHandle(
	IN	INT		iProc
	);

};