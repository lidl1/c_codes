#include "CRegedit.h"

CRegedit::CRegedit(
	VOID
	)
{
}

CRegedit::~CRegedit(
	VOID
	)
{
}

BOOL
CRegedit::ShowRegList(
	string strPath
	)
{
	BOOL bStatus = FALSE;
	do
	{
		HKEY hMainKey = HKEY_LOCAL_MACHINE;
		HKEY hSubKey = NULL;
		
		LONG returnValue = RegOpenKeyEx( hMainKey, strPath.c_str(), 0, 
										 KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE | KEY_WOW64_64KEY, &hSubKey );
		if( ERROR_SUCCESS != returnValue )
		{
			printf("openKey error\n");
			break;
		}

		DWORD dwSubKeyCount = 0;	//子键数	
		DWORD dwKeyvalueCount = 0;	//键值数

		//获取子健数
		returnValue = RegQueryInfoKey( hSubKey, NULL, NULL, NULL, &dwSubKeyCount,
									   NULL, NULL, &dwKeyvalueCount, NULL, NULL, NULL, NULL );
		if( ERROR_SUCCESS != returnValue )
		{
			printf("query sonkey`s number error\n");
			break;
		}

		CHAR tcKeyname[128] = { 0 }; //子健名
		DWORD dwKeyNameSize = 128;	 //长度

		for( DWORD i = 0; i < dwSubKeyCount; i++ )
		{
		
			returnValue = RegEnumKeyEx( hSubKey, i, tcKeyname, &dwKeyNameSize, NULL, NULL, NULL, NULL );
			if( ERROR_SUCCESS != returnValue )
			{
				printf("eunm key error\n");
				break;
			}

			cout << tcKeyname << endl;
			dwKeyNameSize = 128;  //恢复长度

			//更新路径
			string strNewPath = strPath + "\\" ;
			strNewPath = strNewPath + string( tcKeyname );

			HKEY hSsubKey = NULL;		//子键的子键
			DWORD dwSsubKeyCount = 0;	//子健的子健数

			returnValue = RegOpenKeyEx( hMainKey, strNewPath.c_str(), 0, 
										KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE|KEY_WOW64_64KEY, &hSsubKey );
			if( ERROR_SUCCESS != returnValue )
			{
				printf("open son`s sonkey error\n");
				break;
			}

			//获取子健的子健数
			returnValue = RegQueryInfoKey( hSsubKey, NULL, NULL, NULL, &dwSsubKeyCount,
										   NULL, NULL, &dwKeyvalueCount,NULL, NULL, NULL, NULL );
			if( ERROR_SUCCESS != returnValue )
			{
				printf("query son`s sonkey`s number error\n");
				break;
			}
			
			//子健仍有子健 递归调用
			if( dwSsubKeyCount != 0 )
			{
				ShowRegList( strNewPath );
			}

			//子健没有子健 遍历键值
			else
			{
				for( DWORD i = 0; i < dwKeyvalueCount; i++ )
				{
					CHAR tcSonKeyname[ 2048 ] = { 0 }; //键值名称
					DWORD dwSonKeyNameSize = 2048;		//键值名长

					returnValue = RegEnumValue( hSsubKey, i, tcSonKeyname, &dwSonKeyNameSize,
												NULL, NULL, NULL, NULL ); //键值的value先设为NULL
					if( ERROR_SUCCESS != returnValue )
					{
						printf("query key value name error\n");
						break;
					}
					cout << "-----" << tcSonKeyname << endl;
					dwSonKeyNameSize = 2048;
				}
			}

			strNewPath.clear();
		}

		bStatus = TRUE;

	}while( FALSE );

	return bStatus;
}

BOOL
CRegedit::CreateReg(
	IN	HKEY		hMainKey,
	IN	string		strPath
)
{
	BOOL bStatus = FALSE;

	do
	{
		HKEY key = NULL;
	
		SECURITY_ATTRIBUTES L;
		L.nLength = sizeof( SECURITY_ATTRIBUTES );
		L.lpSecurityDescriptor = NULL;
		L.bInheritHandle = TRUE;

		DWORD dwNum = 0;
		long returnValue = RegCreateKeyEx( hMainKey, strPath.c_str(), 0, NULL, 0, KEY_WOW64_64KEY | KEY_ALL_ACCESS, &L, &key, &dwNum );
		if( ERROR_SUCCESS != returnValue )
		{
			printf("create key error\n");
			break;
		}

		bStatus = TRUE;

	}while( FALSE );

	return bStatus;
}

BOOL
CRegedit::SetRegValue(
	IN HKEY			hMainKey,
	IN string		strPath,
	IN string		strValueName,
	IN string		strValue,
	IN DWORD		dwType
	)
{
	BOOL bStatus = FALSE;

	do
	{
		HKEY hKey = NULL;
		long returnValue = RegOpenKeyEx( hMainKey,  strPath.c_str(), 0, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &hKey );
		if( ERROR_SUCCESS != returnValue )
		{
			printf("open key error\n");
			break;
		}

		BYTE* pdata = ( BYTE * )strValue.c_str(); //键值
		INT isize = 128;						   //长度

		returnValue = RegSetValueEx( hKey, strValueName.c_str(), 0, dwType, pdata, isize );
		if( ERROR_SUCCESS != returnValue )
		{
			printf("set value error\n");
			break;
		}

		bStatus = TRUE;

	}while( FALSE );

	return bStatus;
}

BOOL
CRegedit::DeleteRegKey(
	IN HKEY		hMainKey,
	IN string	strPath
)
{
	BOOL bStatus = FALSE;

	do
	{
		LONG returnValue = RegDeleteKey( hMainKey, strPath.c_str() );
		if( ERROR_SUCCESS != returnValue )
		{
			printf("delete key error\n");
			break;
		}
		
		bStatus = TRUE;

	}while( FALSE );

	return bStatus;
}
BOOL
CRegedit::DeleteRegValue(
	IN HKEY			hMainKey,
	IN string		strPath,
	IN string		strValueName
	)
{
	BOOL bStatus = FALSE;

	do
	{
		HKEY hSubKey = NULL;
		
		LONG returnValue = RegOpenKeyEx( hMainKey, strPath.c_str(), 0, 
										 KEY_ALL_ACCESS | KEY_WOW64_64KEY, &hSubKey );
		if( ERROR_SUCCESS != returnValue )
		{
			printf("open key error\n");
			break;
		}

		returnValue = RegDeleteValue( hSubKey, strValueName.c_str() );
		if( ERROR_SUCCESS != returnValue )
		{
			printf("delete value error\n");
			break;
		}
		
		bStatus = TRUE;

	}while( FALSE );

	return bStatus;
}
