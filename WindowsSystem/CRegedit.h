#include "stdafx.h"

class CRegedit
{
	CRegedit();
	~CRegedit();

public:

static BOOL
ShowRegList(
	IN string	strPath
	);

static BOOL
CreateReg(
	IN HKEY		hMainKey,
	IN string	strPath
	);
	
static BOOL
SetRegValue(
	IN HKEY		hMainKey,
	IN string	strPath,
	IN string	strValueName,
	IN string	strValue,
	IN DWORD	dwType
	);

static BOOL
DeleteRegKey(
	IN HKEY		hMainKey,
	IN string	strPath
	);

static BOOL
DeleteRegValue(
	IN HKEY		hMainKey,
	IN string	strPath,
	IN string	strValueName
	);

static BOOL
DeleteRegTree(
	IN HKEY			rootKey,
	IN CString		szRegPath,
	IN CString      deletename
	);

};