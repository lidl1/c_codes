#include "CServer.h"

CServer::CServer(
	VOID
	)
{
}

CServer::~CServer(
	VOID
	)
{
}

BOOL
CServer::ShowServ(
	VOID
	)
{
	BOOL bStatus = FALSE;

	do
	{
		SC_HANDLE hsc = NULL;
		hsc = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS );
		if( NULL == hsc )
		{
			printf("openscmanager error\n");
			break;
		}
	
		DWORD dwBufSize = 0;
		DWORD dwBufNeed = 0;
		DWORD dwNumberOfService = 0;
	
		//获取buf长度 bStatus为FALSE,返回TRUE为异常
		bStatus = EnumServicesStatusEx( hsc, SC_ENUM_PROCESS_INFO, SERVICE_WIN32, SERVICE_STATE_ALL,
										NULL, dwBufSize, &dwBufNeed, &dwNumberOfService, NULL, NULL );
		if( bStatus )
		{
			printf("EnumServicesStatusEx first error\n");
			break;
		}

		dwBufSize = dwBufNeed + sizeof( ENUM_SERVICE_STATUS_PROCESS );

		CHAR* pbuf = NULL;
		pbuf = ( CHAR* )malloc( dwBufSize );
		if( NULL == pbuf )
		{
			printf("pbuf malloc error\n");
			break;
		}

		memset( pbuf, 0, dwBufSize );

		//获取服务信息
		bStatus = EnumServicesStatusEx( hsc, SC_ENUM_PROCESS_INFO, SERVICE_WIN32, SERVICE_STATE_ALL,
										(PBYTE)pbuf, dwBufSize, &dwBufNeed, &dwNumberOfService, NULL, NULL );
		if( !bStatus )
		{
			printf("EnumServicesStatusEx second error\n");
			break;
		}
		bStatus = FALSE;

		ENUM_SERVICE_STATUS_PROCESS *pServiceInfo = NULL;
		pServiceInfo = (LPENUM_SERVICE_STATUS_PROCESS)pbuf;

		//输出服务信息
		for( DWORD i = 0; i < dwNumberOfService; i++ )
		{
			cout << pServiceInfo[i].lpServiceName << ends << pServiceInfo[i].lpDisplayName << ends << pServiceInfo[i].ServiceStatusProcess.dwProcessId << endl;
			//判断服务状态
			switch ( pServiceInfo[i].ServiceStatusProcess.dwCurrentState )
			{
				case SERVICE_RUNNING:
				case SERVICE_START_PENDING:
				{
					cout << "running" << endl;
					break;
				}

				case SERVICE_CONTINUE_PENDING:
				case SERVICE_PAUSE_PENDING:

				case SERVICE_PAUSED:

				case SERVICE_STOP_PENDING:
				case SERVICE_STOPPED:
				{
					cout << "stop" << endl;
					break;
				}
			
				default:
				{
					cout << "dont know" << endl;
					break;
				}
			}

			//获取路径
			SC_HANDLE sevice = NULL;
			LPQUERY_SERVICE_CONFIG pQsc = NULL;

			sevice = OpenService( hsc, pServiceInfo[i].lpServiceName, SERVICE_QUERY_CONFIG );
			if( NULL == sevice )
			{
				printf("openservice error\n");
				break;
			}
			
			//获取空间大小 bStatus为FALSE,返回TRUE为异常
			bStatus = QueryServiceConfig( sevice, pQsc, 0, &dwBufSize );
			if( bStatus )
			{
				printf("QueryServiceConfig first error\n");
				break;
			}
		
			pQsc = ( LPQUERY_SERVICE_CONFIG )malloc( dwBufSize );
			ZeroMemory( pQsc, dwBufSize );

			//获取路径信息
			bStatus = QueryServiceConfig( sevice, pQsc, dwBufSize, &dwBufSize );
			if( !bStatus )
			{
				printf("QueryServiceConfig second error\n");
				break;
			}

			//cout << pQsc->dwStartType << endl; 服务启动类型
			cout << pQsc->lpBinaryPathName << ends << endl;		

			CloseServiceHandle( sevice );
			free( pQsc );
		}

	cout << "service num:" << ends << dwNumberOfService << endl;

	CloseServiceHandle( hsc );
	free( pbuf );

	} while( FALSE );

	return bStatus;
}


BOOL
CServer::StartServ(
	string strName
	)
{
	BOOL bStatus = FALSE;

	do
	{
		SC_HANDLE hsc = NULL;
		SC_HANDLE hService = NULL;

		hsc = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS );
		if( NULL == hsc )
		{
			printf("openscmanager error\n");
			break;
		}

		hService = OpenService( hsc, strName.c_str(), SERVICE_RUNNING | SERVICE_STOPPED | SERVICE_START );
		if( NULL == hService )
		{
			printf("openservice error\n");
			break;
		}
		
		//开启服务
		bStatus = StartService( hService, 0, NULL );
		if( !bStatus )
		{
			printf("startservice error\n");
			break;
		}

		CloseServiceHandle( hsc );
		CloseServiceHandle( hService );

	}while( FALSE );

	return bStatus;
}

BOOL
CServer::StopServ(
	string strName
	)
{
	BOOL bStatus = FALSE;
	
	do
	{
		SC_HANDLE hsc = NULL;
		SC_HANDLE hService = NULL;

		hsc = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS );
		if( NULL == hsc )
		{
			printf("openscmanager error\n");
			break;
		}

		hService = OpenService( hsc, strName.c_str(),  SERVICE_RUNNING | SERVICE_STOPPED | SERVICE_START );
		if( NULL == hService )
		{
			printf("openservice error\n");
			break;
		}

		SERVICE_STATUS status = { 0 };
		bStatus = ControlService( hService, SERVICE_CONTROL_STOP, &status );
		if( !bStatus )
		{
			printf("ControlService error\n");
			break;
		}

		//DeleteService( hservice );
		CloseServiceHandle( hsc );
		CloseServiceHandle( hService );

	} while( FALSE );

	return bStatus;
}