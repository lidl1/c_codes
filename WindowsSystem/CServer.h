#include "stdafx.h"

class CServer{
	
	CServer();
	~CServer();

public:

static BOOL 
ShowServ(
	VOID
	);

static BOOL
StopServ(
	IN string	strName
	);

static BOOL
StartServ(
	IN string	strName
	);

};