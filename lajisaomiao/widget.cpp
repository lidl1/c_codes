#include "widget.h"
#include "ui_widget.h"

CWidget::CWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    setWindowTitle( "垃圾清理" );

    this->setStyleSheet( "background-color:white;" );
    ui->label->setPixmap( QPixmap( ":/5219.PNG" ) );

    ui->treeWidget->setFrameShape( QListWidget::NoFrame );
    ui->pushButton->setStyleSheet( "QPushButton{ background-color:rgb( 255, 165, 0 );"
                                    "color:rgb(255,255,255);"
                                    "border-radius:3px;"
                                    "border: 0px outset rgb(128, 128, 128 );"
                                    "font:bold 15px;"
                                    "}"
                                    "QPushButton:pressed{"
                                    "background-color:rgb(255,113,18);"
                                    "color:rgb(0,255,255);"
                                    "}");

    QFont font( "Microsoft YaHei", 20, 0 );
    ui->label_2->setFont( font );
    QFont font2( "Microsoft YaHei", 10, 25 );
    ui->label_3->setFont( font2 );
    ui->label_3->setStyleSheet( "color:gray;" );
    QFont font3( "Microsoft YaHei" );
    ui->pushButton->setFont( font3 );
    QFont font4( "Microsoft YaHei", 11, 0 );
    ui->treeWidget->setFont( font4 );
    //ui->treeWidget->setStyleSheet( "color:gray;" );

    ui->pushButton->setText( "开始扫描" );

    ui->label_2->setText( "垃圾清理，释放电脑空间" );
    ui->label_3->setText( "请选择需要扫描的垃圾分类" );

    setFixedSize( this->width(), this->height() );
    m_bStatus = TRUE;
    connect( ui->pushButton, QPushButton::clicked, this, CWidget::caozuo );
}


void
CWidget::caozuo(
        VOID
        )
{
    //QString strTmpSuffix = "tmp";
    //QString strLogSuffix = "log";
    //QString strGidSuffix = "gid";
    //QString strChkSuffix = "chk";
    //QString strOldSuffix = "old";
    //QString strBakSuffix = "bak";
    if( m_bStatus )
    {
        ui->treeWidget->clear();
        ui->pushButton->setText( "清理垃圾" );
        QTreeWidgetItem* pXiT = new QTreeWidgetItem( QStringList()<<"系统垃圾" );

        QIcon icon( QPixmap( ":/5220.PNG" ) );
        pXiT->setIcon( 0,icon );

        ui->treeWidget->addTopLevelItem( pXiT );
        XiTongScan( pXiT );

        QTreeWidgetItem* pReg = new QTreeWidgetItem( QStringList()<<"注册表垃圾" );

        QIcon icon1( QPixmap( ":/5221.PNG" ) );
        pReg->setIcon( 0,icon1 );

        ui->treeWidget->addTopLevelItem( pReg );
        RegScan( pReg );

        QTreeWidgetItem* pWeb = new QTreeWidgetItem( QStringList()<<"上网浏览" );

        QIcon icon2( QPixmap( ":/5222.PNG" ) );
        pWeb->setIcon( 0,icon2 );

        ui->treeWidget->addTopLevelItem( pWeb );
        WebScan( pWeb );

       m_bStatus = FALSE;
    }
    else
    {

        ui->pushButton->setText( "开始扫描" );

        DeleteIt();
        ui->treeWidget->clear();

        QTreeWidgetItem* pXiT = new QTreeWidgetItem( QStringList()<<"系统垃圾" );

        QIcon icon( QPixmap( ":/5220.PNG" ) );
        pXiT->setIcon( 0,icon );

        ui->treeWidget->addTopLevelItem( pXiT );
        XiTongScan( pXiT );

        QTreeWidgetItem* pReg = new QTreeWidgetItem( QStringList()<<"注册表垃圾" );

        QIcon icon1( QPixmap( ":/5221.PNG" ) );
        pReg->setIcon( 0,icon1 );

        ui->treeWidget->addTopLevelItem( pReg );
        RegScan( pReg );

        QTreeWidgetItem* pWeb = new QTreeWidgetItem( QStringList()<<"上网浏览" );

        QIcon icon2( QPixmap( ":/5222.PNG" ) );
        pWeb->setIcon( 0,icon2 );

        ui->treeWidget->addTopLevelItem( pWeb );
        WebScan( pWeb );

        m_bStatus = TRUE;
    }
}

void
CWidget::XiTongScan(
        IN QTreeWidgetItem*         pXiT
        )
{
    QTreeWidgetItem* pItem = new QTreeWidgetItem( QStringList()<<"系统缓存" );
    pXiT->addChild( pItem );     
    HuanCunScan( pItem );

    QTreeWidgetItem* pItem2 = new QTreeWidgetItem( QStringList()<<"系统临时文件" );
    pXiT->addChild( pItem2 );  
    TempScan( pItem2 );

    QTreeWidgetItem* pItem3 = new QTreeWidgetItem( QStringList()<<"系统日志" );
    pXiT->addChild( pItem3 );
    LogScan( pItem3 );

    connect( ui->treeWidget, SIGNAL( itemChanged( QTreeWidgetItem*, int ) ), this, SLOT( ChangeStatus( QTreeWidgetItem*, int ) ) );
}

void
CWidget::HuanCunScan(
        IN QTreeWidgetItem*         pHuC
        )
{
    QTreeWidgetItem* pYudu = new QTreeWidgetItem( QStringList()<<"预读文件缓存" );
    pHuC->addChild( pYudu );
    pYudu->setCheckState( 0, Qt::Unchecked );
    MakeList( "C:\\Windows\\prefetch", pYudu );

    QTreeWidgetItem* pSuolue = new QTreeWidgetItem( QStringList()<<"缩略图缓存" );
    pHuC->addChild( pSuolue );
    pSuolue->setCheckState( 0, Qt::Unchecked );
    MakeList( "C:\\Users\\Administrator\\AppData\\Local\\Microsoft\\Windows\\Explorer", pSuolue );

}

void
CWidget::TempScan(
        IN QTreeWidgetItem*         pTemp
        )
{
    QTreeWidgetItem* pLinshi = new QTreeWidgetItem( QStringList()<<"临时文件" );
    pTemp->addChild( pLinshi );
    pLinshi->setCheckState( 0, Qt::Unchecked );
    MakeList1( "C:\\Users\\Administrator\\AppData\\Local\\Temp", pLinshi );

    QTreeWidgetItem* pRecent = new QTreeWidgetItem( QStringList()<<"最近打开文件" );
    pTemp->addChild( pRecent );
    pRecent->setCheckState( 0, Qt::Unchecked );
    MakeList( "C:\\Users\\Administrator\\AppData\\Roaming\\Microsoft\\Windows\\Recent", pRecent );

    QTreeWidgetItem* pJump= new QTreeWidgetItem( QStringList()<<"Windows跳转列表" );
    pTemp->addChild( pJump );
    pJump->setCheckState( 0, Qt::Unchecked );
    MakeList2( "C:\\Users\\Administrator\\AppData\\Roaming\\Microsoft\\Windows\\Recent\\CustomDestinations", pJump );
}

void
CWidget::LogScan(
        IN QTreeWidgetItem*         pLog
        )
{
    QTreeWidgetItem* pbuding = new QTreeWidgetItem( QStringList()<<"自动更新补丁日志" );
    pLog->addChild( pbuding );
    pbuding->setCheckState( 0, Qt::Unchecked );
    MakeList1( "C:\\Windows\\SoftwareDistribution\\DataStore", pbuding );

    QTreeWidgetItem* pError = new QTreeWidgetItem( QStringList()<<"错误报告" );
    pLog->addChild( pError );
    pError->setCheckState( 0, Qt::Unchecked );
    MakeList1( "C:\\Users\\Administrator\\AppData\\Local\\Microsoft\\Windows\\WER\\ReportArchive", pError );

    QTreeWidgetItem* pSearch = new QTreeWidgetItem( QStringList()<<"Search日志" );
    pLog->addChild( pSearch );
    pSearch->setCheckState( 0, Qt::Unchecked );
    MakeList( "C:\\ProgramData\\Microsoft\\Search\\Data\\Applications\\Windows", pSearch );

    QTreeWidgetItem* pSystem = new QTreeWidgetItem( QStringList()<<"系统日志文件" );
    pLog->addChild( pSystem );
    pSystem->setCheckState( 0, Qt::Unchecked );
    MakeList2( "C:\\Windows\\System32\\LogFiles\\Scm", pSystem );
}

void
CWidget::MakeList(
        IN QString              strPath,
        IN QTreeWidgetItem*     pRootItem
        )
{
    QString strRoot = strPath;
    QDir rootDir( strRoot );
    QStringList strList;
    strList<<"*";
    QFileInfoList fileList = rootDir.entryInfoList( strList );

    for( int i = 0; i < fileList.size(); i++ )
    {
        if("." ==  fileList.at( i ).fileName()||
           ".." == fileList.at( i ).fileName() )
        {
            continue;
        }
        else
        {
            if( "db"  ==  fileList.at( i ).suffix()||
                "pf"  ==  fileList.at( i ).suffix()||
                "lnk" ==  fileList.at( i ).suffix()||
                "jrs" ==  fileList.at( i ).suffix() )
            {
                QTreeWidgetItem* pItem = new QTreeWidgetItem( QStringList()<<fileList.at(i).absolutePath()+"/"+fileList.at(i).fileName() );
                if( Qt::Checked == pRootItem->checkState( 0 ) )
                {
                    pItem->setCheckState( 0, Qt::Checked );
                }
                else
                {
                    pItem->setCheckState( 0, Qt::Unchecked );
                }
                pRootItem->addChild( pItem );

            }
        }
    }
}


void
CWidget::MakeList1(
        IN QString              strPath,
        IN QTreeWidgetItem*     pRootItem
        )
{
    QString strRoot = strPath;
    QDir rootDir( strRoot );
    QStringList strList;
    strList<<"*";
    QFileInfoList fileList = rootDir.entryInfoList( strList );

    if( 0 < fileList.size() )
    {
        for( INT i = 0; i < fileList.size(); i++ )
        {
            if("." ==  fileList.at( i ).fileName()||
               ".." == fileList.at( i ).fileName() )
            {
                continue;
            }
            QString strNewPath =  strPath + "\\" + fileList.at( i ).fileName();
            if( QDir( strNewPath ).exists() )
            {
                MakeList1( strNewPath, pRootItem );
            }
            else
            {
                QTreeWidgetItem* pItem = new QTreeWidgetItem( QStringList()<<fileList.at(i).absolutePath()+"/"+fileList.at(i).fileName() );
                //qDebug()<<fileList.at(i).fileName();
                pItem->setCheckState( 0, Qt::Unchecked );
                pRootItem->addChild( pItem );
            }
        }
    }
}

void
CWidget::MakeList2(
        IN QString              strPath,
        IN QTreeWidgetItem*     pRootItem
        )
{
    QString strRoot = strPath;
    QDir rootDir( strRoot );
    QStringList strList;
    strList<<"*";
    QFileInfoList fileList = rootDir.entryInfoList( strList );

    for( int i = 0; i < fileList.size(); i++ )
    {
        if("." ==  fileList.at( i ).fileName()||
           ".." == fileList.at( i ).fileName() )
        {
            continue;
        }
        else
        {
            QTreeWidgetItem* pItem = new QTreeWidgetItem( QStringList()<<fileList.at(i).absolutePath()+"/"+fileList.at(i).fileName() );
            pItem->setCheckState( 0, Qt::Unchecked );

            pRootItem->addChild( pItem );
        }
    }
}

void
CWidget::RegScan(
        IN QTreeWidgetItem*     pReg
        )
{
    QTreeWidgetItem* pItem = new QTreeWidgetItem( QStringList()<<"DLL相关" );
    pReg->addChild( pItem );

    DllScan( pItem );

    QTreeWidgetItem* pItem2 = new QTreeWidgetItem( QStringList()<<"程序相关" );
    pReg->addChild( pItem2 );

    ChengXvScan( pItem2 );
}

void
CWidget::WebScan(
        IN QTreeWidgetItem*     pWeb
        )
{
    QTreeWidgetItem* pItem = new QTreeWidgetItem( QStringList()<<"IE浏览器" );
    pWeb->addChild( pItem );

    IEScan( pItem );
}

void
CWidget::DllScan(
        IN QTreeWidgetItem*     pDll
        )
{
    QTreeWidgetItem* pItem = new QTreeWidgetItem( QStringList()<<"缺失共享Dll" );
    pDll->addChild( pItem );
    pItem->setCheckState( 0, Qt::Unchecked );

    MakeDllList( HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\SharedDLLs"), pItem );
    MakeDllList( HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\SharedDLLs"), pItem );

}

void
CWidget::IEScan(
        IN QTreeWidgetItem*     pIE
        )
{
    QTreeWidgetItem* pItem = new QTreeWidgetItem( QStringList()<<"IE Cookies" );
    pIE->addChild( pItem );
    pItem->setCheckState( 0, Qt::Unchecked );

    MakeList1( "C:\\Users\\Administrator\\AppData\\Roaming\\Microsoft\\Windows\\Cookies", pItem );

//    QTreeWidgetItem* pItem2 = new QTreeWidgetItem( QStringList()<<"IE 浏览器缓存" );
//    pIE->addChild( pItem2 );
//    MakeList1( "C:\\Users\\Administrator\\AppData\\Local\\Microsoft\\Windows\\Temporary Internet Files\\Content.IE5", pItem2 );

}

void
CWidget::MakeDllList(
        IN HKEY                 hKey,
        IN LPCWSTR              strPath,
        IN QTreeWidgetItem*     pRootItem
        )
{
    HKEY  hSubKey;
    DWORD dwKeyValueCount = 0;
    WCHAR tcKeyName[1280] = { 0 };
    DWORD dwKeyNameSize = 128;
    WCHAR tcKeyValue[500] = { 0 };
    DWORD dwKeyValueSize = 256;

    LONG lReturn = RegOpenKeyEx( hKey, strPath, 0, KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE | KEY_WOW64_64KEY, &hSubKey );
    if( ERROR_SUCCESS != lReturn )
    {
        return;
    }

    lReturn = RegQueryInfoKey( hSubKey, NULL, NULL, NULL, NULL, NULL, NULL, &dwKeyValueCount, NULL, NULL, NULL, NULL );
    if( ERROR_SUCCESS != lReturn )
    {
        return;
    }

    for( INT i = 0; i < dwKeyValueCount; i++ )
    {
        RegEnumValue( hSubKey, i, tcKeyName, &dwKeyNameSize, NULL, NULL, (LPBYTE)tcKeyValue, &dwKeyValueSize );

        QString strKeyName = QString::fromWCharArray( tcKeyName );
        //qDebug() << strKeyName;
        BOOL bStatus = isFileExist( strKeyName );
        if( !bStatus )
        {
            //qDebug()<<strKeyName;
            QTreeWidgetItem* pItem = new QTreeWidgetItem( QStringList()<<strKeyName );
            pItem->setCheckState( 0, Qt::Unchecked );
            pRootItem->addChild( pItem );
        }
        dwKeyNameSize = 128;

    }
}

void
CWidget::ChengXvScan(
        IN QTreeWidgetItem*     pChengX
        )
{
    QTreeWidgetItem* pItem = new QTreeWidgetItem( QStringList()<<"最近使用痕迹" );
    pChengX->addChild( pItem );
    pItem->setCheckState( 0, Qt::Unchecked );

    MakeUserList( "HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\RunMRU", pItem );
    MakeUserList( "HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\TypedPaths", pItem );
}

void
CWidget::MakeUserList(
        IN QString              strPath,
        IN QTreeWidgetItem *    pRootItem
        )
{
    QTreeWidgetItem* pItem = new QTreeWidgetItem( QStringList()<<strPath );
    pItem->setCheckState( 0, Qt::Unchecked );

    pRootItem->addChild( pItem );
}

BOOL
CWidget::isFileExist(
        IN QString          strFilePath
        )
{
    QFileInfo fileInfo( strFilePath );

    if( fileInfo.isFile() ||
        "exe" == fileInfo.suffix() )
    {
        return TRUE;
    }

    return FALSE;
}
void
CWidget::ChangeStatus(
        IN QTreeWidgetItem*      pItem,
        IN int                   iColumn
        )
{
    for( int i = 0; i < pItem->childCount(); i++ )
    {
        pItem->child( i )->setCheckState( 0, pItem->checkState( 0 ) );
    }
}

void
CWidget::DeleteIt()
{
  QTreeWidgetItem* pItem= ui->treeWidget->topLevelItem( 0 );
  qDebug()<<pItem->text( 0 );
  for( int i = 0; i < pItem->childCount(); i++ )
  {
      QTreeWidgetItem* pItem1 = pItem->child( i );
      for( int j = 0; j < pItem1->childCount(); j++ )
      {
          QTreeWidgetItem* pItem2 = pItem1->child( j );
          if( Qt::Checked == pItem2->checkState(0) )
          {
              for( int ij = 0; ij < pItem2->childCount(); ij++ )
              {
                  QTreeWidgetItem* pItem3 = pItem2->child( ij );
                  QString strPath = pItem3->text( 0 );
                  //qDebug()<<strPath;
                  QFile file( strPath );
                  file.remove();
//                LPWSTR str;
//                strPath.toWCharArray( str );
//                DeleteFile( str );
              }
          }
      }
  }

}

CWidget::~CWidget()
{
    delete ui;
}
