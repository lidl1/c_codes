#ifndef WIDGET_H
#define WIDGET_H
#include <QWidget>
#include <QPalette>
#include <QDebug>
#include <windows.h>
#include <QListWidget>
#include <QListWidgetItem>
#include <QFileIconProvider>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QComboBox>
#include <QSettings>
#include <shlobj.h>
#include <string.h>
#include <iostream>
#include <shlobj.h>
#include <shlguid.h>
#include <shellapi.h>
#include <QDir>
#include <QFileInfoList>
#include <QStringList>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QPixmap>

using namespace  std;

namespace Ui {
class Widget;
}

class CWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CWidget(QWidget *parent = 0);
    ~CWidget();

    QString          m_strRoot;
    QString          m_strPath;
    QFileInfoList    m_fileList;
    BOOL             m_bStatus;

void
MakeList(
        IN QString              strPath,
        IN QTreeWidgetItem*     pRootItem
        );

void
MakeList1(
        IN QString              strPath,
        IN QTreeWidgetItem*     pRootItem
        );

void
MakeList2(
        IN QString              strPath,
        IN QTreeWidgetItem*     pRootItem
        );

void
MakeDllList(
        IN HKEY                 hKey,
        IN LPCWSTR              strPath,
        IN QTreeWidgetItem*     pRootItem
        );

void
MakeUserList(
        IN QString              strPath,
        IN QTreeWidgetItem*     pRootItem
        );

void
XiTongScan(
        IN QTreeWidgetItem*     pXiT
        );

void
HuanCunScan(
        IN QTreeWidgetItem*     pHuC
        );

void
TempScan(
        IN QTreeWidgetItem*     pTemp
        );

void
LogScan(
        IN QTreeWidgetItem*     pLog
        );

void
RegScan(
        IN QTreeWidgetItem*     pReg
        );

void
WebScan(
        IN QTreeWidgetItem*     pWeb
        );

void
IEScan(
        IN QTreeWidgetItem*     pIE
        );

void
DllScan(
        IN QTreeWidgetItem*     pDll
        );

void
ChengXvScan(
        IN QTreeWidgetItem*     pChengX
        );

BOOL
isFileExist(
        IN QString              strFilePath
        );
void
DeleteIt(
        void
        );

private:
    Ui::Widget *ui;


public slots:
    void caozuo();  
    void ChangeStatus( QTreeWidgetItem* pItem, int column );

};

#endif // WIDGET_H
