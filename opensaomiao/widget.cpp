#include "widget.h"
#include "ui_widget.h"

CWidget::CWidget(
        QWidget *parent
        ) : QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    setWindowTitle( "启动优化" );
    this->setStyleSheet( "background-color:white;" );

    ui->label_3->setPixmap( QPixmap( ":/5215.PNG" ) );
    ui->tabWidget->setStyleSheet( "QTabBar::tab{ height: 40px; width: 150px; }" );
    ui->tabWidget->setDocumentMode( true );
    ui->listWidget->setFrameShape( QListWidget::NoFrame );
    ui->listWidget_2->setFrameShape( QListWidget::NoFrame );
    ui->pushButton->setStyleSheet( "QPushButton{ background-color:rgb( 255, 165, 0 );"
                                    "color:rgb(255,255,255);"
                                    "border-radius:3px;"
                                    "border: 0px outset rgb(128, 128, 128 );"
                                    "font:bold 15px;"
                                    "}"
                                    "QPushButton:pressed{"
                                    "background-color:rgb(255,113,18);"
                                    "color:rgb(0,255,255);"
                                    "}");

    QFont font( "Microsoft YaHei", 20, 0 );
    ui->label->setFont( font );
    QFont font2( "Microsoft YaHei", 10, 25 );
    ui->label_2->setFont( font2 );
    ui->label_2->setStyleSheet( "color:gray;" );
    QFont font3( "Microsoft YaHei" );
    ui->pushButton->setFont( font3 );
    QFont font4( "Microsoft YaHei", 11, 0 );
    ui->tabWidget->setFont( font4 );
    ui->tabWidget->setStyleSheet( "color:gray;" );

    ui->label->setText( "未发现可优化启动项目" );
    ui->label_2->setText( "手动管理启动项目，提高开机速度。" );

    SaoMiao();

    connect( ui->pushButton, QPushButton::clicked, this, CWidget::SaoMiao );

    setFixedSize( this->width(), this->height() );
}

void
CWidget::SaoMiao(
        void
        )
{
    SaoMiaoService();
    SaoMiaoStart();

}


void
CWidget::SaoMiaoService(
        void
        )
{

    ui->listWidget_2->clear();
    ui->listWidget_2->setStyleSheet( "color:gray;" );

    SC_HANDLE hsc = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS );
    SC_HANDLE sh = NULL;
    CHAR* pbuf = NULL;
    DWORD dwBufSize = 0;
    DWORD dwBufNeed = 0;
    DWORD dwNumberOfService = 0;
    ENUM_SERVICE_STATUS_PROCESS *pServiceInfo = NULL;
    LPQUERY_SERVICE_CONFIG pQsc = NULL;

    EnumServicesStatusEx( hsc, SC_ENUM_PROCESS_INFO, SERVICE_WIN32, SERVICE_STATE_ALL, NULL, dwBufSize, &dwBufNeed, &dwNumberOfService, NULL, NULL );
    dwBufSize = dwBufNeed + sizeof(ENUM_SERVICE_STATUS_PROCESS);
    pbuf = (CHAR*)malloc( dwBufSize );
    memset( pbuf, 0, dwBufSize );

    EnumServicesStatusEx( hsc, SC_ENUM_PROCESS_INFO, SERVICE_WIN32, SERVICE_STATE_ALL, (PBYTE)pbuf, dwBufSize, &dwBufNeed, &dwNumberOfService, NULL, NULL );
    pServiceInfo = ( LPENUM_SERVICE_STATUS_PROCESS )pbuf;

    for( INT i = 0; i < dwNumberOfService; i++ )
    {
        // qDebug() << pServiceInfo[i].lpServiceName << " " << pServiceInfo[i].lpDisplayName << " " << pServiceInfo[i].ServiceStatusProcess.dwProcessId << endl;
        QString strMsg = QString::fromWCharArray( pServiceInfo[i].lpServiceName );
        qDebug()<<strMsg;

        m_strName = QString::fromWCharArray( pServiceInfo[i].lpDisplayName );
        //qDebug()<<m_strName;

        LPSERVICE_DESCRIPTION pDesc = NULL;
        pDesc = ( LPSERVICE_DESCRIPTION )LocalAlloc( LPTR, 4096 );

        sh = OpenService( hsc, pServiceInfo[i].lpServiceName, SERVICE_QUERY_CONFIG );
        if( NULL == pDesc )
        {
            qDebug()<<"1";
        }

        DWORD dwNeed = 4096;

        QueryServiceConfig2( sh, SERVICE_CONFIG_DESCRIPTION, (LPBYTE)pDesc, 4096, &dwNeed );
        QString str = QString::fromWCharArray( pDesc->glpDescription );
        //qDebug()<<str;
        //qDebug() << dwNumberOfService;

        SC_HANDLE sevice = OpenService( hsc, pServiceInfo[i].lpServiceName, SERVICE_QUERY_CONFIG );
        QueryServiceConfig( sevice, pQsc, 0, &dwBufSize );

        pQsc = (LPQUERY_SERVICE_CONFIG)malloc( dwBufSize );
        ZeroMemory( pQsc, dwBufSize );

        QueryServiceConfig( sevice, pQsc, dwBufSize, &dwBufSize );
        //qDebug()<< pQsc->dwStartType << " " << pQsc->lpBinaryPathName << " " << endl;
        QString strPath = QString::fromWCharArray( pQsc->lpBinaryPathName );

        QString strFilePath = getPath( strPath );
        qDebug()<<strFilePath<<endl<<strPath;

        QListWidgetItem* pItem = new QListWidgetItem( ui->listWidget_2 );

        //qDebug()<<i;
        makeList( ui->listWidget_2, pItem, strMsg, str, strFilePath, pServiceInfo[i].ServiceStatusProcess.dwCurrentState, i, pQsc->dwStartType );

    }
}

void
CWidget::SaoMiaoStart(
       void
        )
{
    ui->listWidget->clear();
    ui->listWidget->setStyleSheet( "color:gray;" );

    INT iIndex = 0;
    SaoMiaoReg( HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"), m_strList, iIndex, FALSE );
    SaoMiaoReg( HKEY_CURRENT_USER, TEXT("software\\Microsoft\\Windows\\CurrentVersion\\Run"), m_strList, iIndex, FALSE );
    SaoMiaoReg( HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Run"), m_strList, iIndex, FALSE );
    SaoMiaoReg( HKEY_LOCAL_MACHINE, TEXT("HARDWARE\\ldl"), m_strList, iIndex, TRUE );
    for( INT i = 0; i < m_strList.size(); i++ )
    {
        qDebug()<<m_strList.at( i );
    }
}

void
CWidget::SaoMiaoReg(
      IN  HKEY            hKey,
      IN  LPCWSTR         chPath,
      OUT QStringList&    strList,
      OUT INT&            iIndex,
      IN  BOOL            bLation
        )
{
    HKEY hMainKey = hKey;
    HKEY hSubKey;

    WCHAR tcKeyname1[1280] = { 0 };
    DWORD dwKeyNameSize1 = 128;
    WCHAR tcKeyvalue[500] = { 0 };
    DWORD dwKeyvalueSize = 256;

    DWORD dwSsubKeyCount = 0;
    DWORD dwKeyvalueCount = 0;


    LONG returnValue = RegOpenKeyEx( hMainKey, chPath, 0, KEY_ENUMERATE_SUB_KEYS | KEY_QUERY_VALUE|KEY_WOW64_64KEY, &hSubKey );
    if( ERROR_SUCCESS == returnValue )
    {
        RegQueryInfoKey( hSubKey, NULL, NULL, NULL, &dwSsubKeyCount, NULL, NULL, &dwKeyvalueCount,NULL, NULL, NULL, NULL );

        for( INT i = 0; i < dwKeyvalueCount; i++ )
        {
            RegEnumValue( hSubKey, i, tcKeyname1, &dwKeyNameSize1, NULL, NULL, (LPBYTE)tcKeyvalue, &dwKeyvalueSize );
            QString strTcKeyName = QString::fromWCharArray( tcKeyname1 );
            qDebug() << strTcKeyName;

            QString strValue = QString::fromWCharArray( tcKeyvalue );
            QString strKeyValue = getPath( strValue );
            //qDebug()<<strValue;

            strList.append( strValue );
            QString strPath = QString::fromWCharArray( chPath );
            m_strPath.append( strPath );

            QIcon icon = setIcon( strKeyValue );
            QListWidgetItem* pItem = new QListWidgetItem( ui->listWidget );
            makeStartList( ui->listWidget, pItem, strTcKeyName, strKeyValue, iIndex, bLation );
            iIndex++;

            dwKeyNameSize1 = 128;
            dwKeyvalueSize = 256;
        }
    }
    RegCloseKey( hMainKey );
    RegCloseKey( hSubKey );
}


QIcon
CWidget::setIcon(
       IN QString       strFilePath
        )
{
    QFileIconProvider fileIP;
    QIcon icon = fileIP.icon( QFileInfo( strFilePath ) );

    return icon;
}

QString
CWidget::getPath(
        IN  QString      strPath
        )
{
    QString strFilePath ;
    QString str;
    BOOL bStatus = FALSE;
    INT iNum = 0;

    for( INT i = 0; i < strPath.size(); i++ )
    {
        if( ' ' == strPath.at( i )&&
               ( '-' == strPath.at( i+1 )||( '/' == strPath.at( i+1 ) ) ) )
        {
            strFilePath = strPath.left( i );
            bStatus = TRUE;
            break;
        }
        else if( "\"" == strPath.at( i ) )
        {
            str = strPath.right( strPath.size() - 1 );
            for( INT j = 1; j < str.size(); j++ )
            {
                iNum++;
                if( "\"" == str.at( j ) )
                {
                    break;
                }
            }

            strFilePath = str.left( iNum );

            bStatus = TRUE;
            break;
        }

    }
    if( !bStatus )
    {
        strFilePath = strPath;
    }

    return strFilePath;
}

void
CWidget::makeList(
        IN QListWidget*            pList,
        IN QListWidgetItem*        pItem,
        IN QString                 strMsg,
        IN QString                 str,
        IN QString                 strFilePath,
        IN DWORD                   dwStatus,
        IN INT                     index,
        IN DWORD                   dwStartType
        )
{


    pItem->setSizeHint( QSize( 100, 100 ) );

    QWidget* pw = new QWidget( pList );
    QHBoxLayout* pLayout = new QHBoxLayout( pw );
    QComboBox*  pBox = new QComboBox( pw );
    pBox->setFixedSize( 90,25 );
    QPixmap pix1( ":/5217.PNG" );
    QIcon icon1( pix1 );
    QPixmap pix2( ":/5216.PNG" );
    QIcon icon2( pix2 );
    pBox->addItem( icon1,"禁止启动" );
    pBox->addItem( icon2,"正在启动" );

    QString strIndex = QString::number( index );
    pBox->setWhatsThis( strIndex );

    if( 4 != dwStartType )
    {
        pBox->setCurrentIndex( 1 );
    }
    pBox->setStyleSheet( "QComboBox {"
                         "border: 1px solid gray;"
                         "}"
                         "QComboBox::drop-down{"
                         "border:none;"
                         "}"
                        );

    connect( pBox, SIGNAL( currentIndexChanged(int) ), this, SLOT( ChangeBySatus(int) )  );

    //qDebug()<<dwStatus;
    QLabel* pLabel1 = new QLabel( pw );
    pLabel1->resize( QSize( 450, 15 ) );
    pLabel1->setText( strMsg );
    QFont font( "Microsoft YaHei", 11, 0 );
    pLabel1->setFont( font );

    QLabel* pLabel2 = new QLabel( pw );

    pLabel2->setFixedSize( QSize( 450, 15 ) );
    pLabel2->setText( str);
    QFont font2( "Microsoft YaHei", 9, 0 );
    pLabel2->setFont( font2 );

    QVBoxLayout* pvBox = new QVBoxLayout( pw );
    pvBox->addWidget( pLabel1 );
    pvBox->addWidget( pLabel2 );
    pvBox->setSpacing( 0 );
    pvBox->setContentsMargins( 20, 20, 20, 20 );

    QLabel* pLabel3 = new QLabel( pw );

    QIcon icon = setIcon( strFilePath );

    QPixmap pix = icon.pixmap( icon.actualSize( QSize( 200, 200 ) ) );
    pLabel3->setPixmap( pix );
    pLabel3->resize( 150, 200 );

    QLabel* pLabel4 = new QLabel( pw );
    pLabel4->setFixedSize( 80, 15 );
    QFont font3( "Microsoft YaHei", 9, 0 );
    pLabel4->setFont( font3 );
    pLabel4->setText( "保持现状" );

    pLayout->addWidget( pLabel3 );
    pLayout->setSpacing( 10 );
    pLayout->setContentsMargins( 30,0,0,10 );
    pLayout->addLayout( pvBox );

    pLayout->addWidget( pLabel4 );
    pLayout->addWidget( pBox );


    pw->setLayout( pLayout );
    pw->show();

    pList->setItemWidget( pItem, pw );
}

void
CWidget::ChangeBySatus(
        IN int          i
        )
{
    if( 0 == i )
    {
        QComboBox*   m_pSenderObj = qobject_cast<QComboBox*>( sender() );
        //qDebug()<<m_pSenderObj->whatsThis();

        int iRow = m_pSenderObj->whatsThis().toInt();
        //qDebug()<<iRow;

        QListWidgetItem* pItem = ui->listWidget_2->item( iRow );
        QWidget* pWidget = ui->listWidget_2->itemWidget( pItem );

        QList<QLabel*> list = pWidget->findChildren<QLabel*>( );
        QList<QLabel*>::iterator itor = list.begin();

        QString strServiceName = (*itor)->text();
        std::wstring wstrName =  strServiceName.toStdWString();
        LPCWSTR strName = wstrName.c_str();

        SC_HANDLE hsc = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS );
        SC_HANDLE hservice = OpenService( hsc, strName, SERVICE_ALL_ACCESS|DELETE );

        if( !ChangeServiceConfig( hservice, SERVICE_NO_CHANGE,SERVICE_DISABLED, SERVICE_NO_CHANGE,
                                NULL,NULL,NULL,NULL,NULL,NULL,NULL) )
        {
            qDebug()<<"error"<<endl;
        }

        SERVICE_STATUS status;
        ControlService(hservice, SERVICE_CONTROL_STOP, &status);
        CloseServiceHandle( hsc );
        CloseServiceHandle( hservice );
    }
    else
    {
        QComboBox*   m_pSenderObj = qobject_cast<QComboBox*>( sender() );

        int iRow = m_pSenderObj->whatsThis().toInt();
        QListWidgetItem* pItem = ui->listWidget_2->item( iRow );
        QWidget* pWidget = ui->listWidget_2->itemWidget( pItem );

        QList<QLabel*> list = pWidget->findChildren<QLabel*>( );
        QList<QLabel*>::iterator itor = list.begin();

        QString strServiceName = (*itor)->text();
        std::wstring wstrName =  strServiceName.toStdWString();
        LPCWSTR strName = wstrName.c_str();

        SC_HANDLE hsc = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS );
        SC_HANDLE hservice = OpenService( hsc, strName, SERVICE_ALL_ACCESS|DELETE );

        if( !ChangeServiceConfig( hservice, SERVICE_NO_CHANGE,SERVICE_AUTO_START, SERVICE_NO_CHANGE,
                                NULL,NULL,NULL,NULL,NULL,NULL,NULL) )
        {
            qDebug()<<"error"<<endl;
        }

        StartService( hservice,0,NULL );
        CloseServiceHandle( hsc );
        CloseServiceHandle( hservice );

    }
}

void
CWidget::makeStartList(
        IN QListWidget*            pList,
        IN QListWidgetItem*        pItem,
        IN QString                 strMsg,
        IN QString                 strIconPath,
        IN INT                     index,
        IN BOOL                    bLaction
        )
{


    pItem->setSizeHint( QSize( 100, 100 ) );

    QWidget* pw = new QWidget( pList );
    QHBoxLayout* pLayout = new QHBoxLayout( pw );
    QComboBox*  pBox = new QComboBox( pw );
    pBox->setFixedSize( 90,25 );
    QPixmap pix1( ":/5217.PNG" );
    QIcon icon1( pix1 );
    QPixmap pix2( ":/5216.PNG" );
    QIcon icon2( pix2 );
    pBox->addItem( icon1,"禁止启动" );
    pBox->addItem( icon2,"正在启动" );

    QString strIndex = QString::number( index );
    pBox->setWhatsThis( strIndex );

    if( !bLaction )
    {
        pBox->setCurrentIndex( 1 );
    }
    pBox->setStyleSheet( "QComboBox {"
                         "border: 1px solid gray;"
                         "}"
                         "QComboBox::drop-down{"
                         "border:none;"
                         "}"
                        );

    connect( pBox, SIGNAL( currentIndexChanged(int) ), this, SLOT( ChangeBySatus1(int) )  );

    //qDebug()<<dwStatus;
    QLabel* pLabel1 = new QLabel( pw );
    pLabel1->resize( QSize( 450, 15 ) );
    pLabel1->setText( strMsg );
    QFont font( "Microsoft YaHei", 11, 0 );
    pLabel1->setFont( font );

    QLabel* pLabel2 = new QLabel( pw );

    pLabel2->setFixedSize( QSize( 450, 15 ) );
    QString stra = "asdasda";
    pLabel2->setText( stra );
    QFont font2( "Microsoft YaHei", 9, 0 );
    pLabel2->setFont( font2 );

    QVBoxLayout* pvBox = new QVBoxLayout( pw );
    pvBox->addWidget( pLabel1 );
    pvBox->addWidget( pLabel2 );
    pvBox->setSpacing( 0 );
    pvBox->setContentsMargins( 20, 20, 20, 20 );

    QLabel* pLabel3 = new QLabel( pw );

    QIcon icon = setIcon( strIconPath );

    QPixmap pix = icon.pixmap( icon.actualSize( QSize( 200, 200 ) ) );
    pLabel3->setPixmap( pix );
    pLabel3->resize( 150, 200 );

    QLabel* pLabel4 = new QLabel( pw );
    pLabel4->setFixedSize( 80, 15 );
    QFont font3( "Microsoft YaHei", 9, 0 );
    pLabel4->setFont( font3 );
    pLabel4->setText( "保持现状" );

    pLayout->addWidget( pLabel3 );
    pLayout->setSpacing( 10 );
    pLayout->setContentsMargins( 30,0,0,10 );
    pLayout->addLayout( pvBox );

    pLayout->addWidget( pLabel4 );
    pLayout->addWidget( pBox );


    pw->setLayout( pLayout );
    pw->show();

    pList->setItemWidget( pItem, pw );
}

void
CWidget::ChangeBySatus1(
        int         i
        )
{

    if( 0 == i )
    {
        QComboBox*   m_pSenderObj = qobject_cast<QComboBox*>( sender() );
        //qDebug()<<m_pSenderObj->whatsThis();

        int iRow = m_pSenderObj->whatsThis().toInt();
        //qDebug()<<iRow;

        HKEY hMain;
        QString strPath = m_strPath.at( iRow );
        if( "s" == strPath.left(1) )
        {
            qDebug()<<"1";
            hMain = HKEY_CURRENT_USER;
        }
        else
        {
            hMain = HKEY_LOCAL_MACHINE;
        }

        QListWidgetItem* pItem = ui->listWidget->item( iRow );
        QWidget* pWidget = ui->listWidget->itemWidget( pItem );
        QList<QLabel*> list = pWidget->findChildren<QLabel*>( );
        QList<QLabel*>::iterator itor = list.begin();

        QString strName = (*itor)->text();
        //qDebug()<<strName;
        qDebug()<<strPath;
        std::wstring wstrPath =  strPath.toStdWString();
        LPCWSTR chPath = wstrPath.c_str();
        HKEY hSubkey;
        RegOpenKeyEx( hMain, chPath, 0, KEY_ENUMERATE_SUB_KEYS | KEY_ALL_ACCESS | KEY_WOW64_64KEY, &hSubkey );

        //qDebug()<<strName;
        std::wstring wstrName =  strName.toStdWString();
        LPCWSTR chName = wstrName.c_str();

        LONG lReturn = RegDeleteValue( hSubkey, chName );

        HKEY hDelete = HKEY_LOCAL_MACHINE;
        HKEY hSub;
        HKEY hSsub;
        QString strValue = m_strList.at( iRow );
        std::wstring wstrValue =  strValue.toStdWString();
        LPCWSTR chValue = wstrValue.c_str();
        DWORD dwLength =  strValue.size();

        RegOpenKeyEx( hDelete, TEXT("HARDWARE\\ldl"), 0, KEY_ALL_ACCESS|KEY_WOW64_64KEY, &hSub );
        RegSetValueEx( hSub, chName, 0, REG_SZ, (PBYTE)chValue, dwLength*3 );

        RegOpenKeyEx( hDelete, TEXT("HARDWARE\\ldl\\ldlRegPath"), 0, KEY_ALL_ACCESS|KEY_WOW64_64KEY, &hSsub );
        RegSetValueEx( hSsub, chName, 0, REG_SZ, (PBYTE)chPath, (strPath.size())*3 );


        RegCloseKey( hMain );
        RegCloseKey( hSubkey );
        RegCloseKey( hDelete );
        RegCloseKey( hSub );
        RegCloseKey( hSsub );
    }
    else
    {
        QComboBox*   m_pSenderObj = qobject_cast<QComboBox*>( sender() );

        int iRow = m_pSenderObj->whatsThis().toInt();

        HKEY hMain = HKEY_LOCAL_MACHINE;
        HKEY hMain2 ;
        QString strPath = m_strPath.at( iRow );
        qDebug()<<strPath;


        QListWidgetItem* pItem = ui->listWidget->item( iRow );
        QWidget* pWidget = ui->listWidget->itemWidget( pItem );
        QList<QLabel*> list = pWidget->findChildren<QLabel*>( );
        QList<QLabel*>::iterator itor = list.begin();

        QString strName = (*itor)->text();
        //qDebug()<<strName;

        std::wstring wstrPath =  strPath.toStdWString();
        LPCWSTR chPath = wstrPath.c_str();
        HKEY hSubkey;
        HKEY hSsubkey;

        RegOpenKeyEx( hMain, TEXT("HARDWARE\\ldl"), 0, KEY_ENUMERATE_SUB_KEYS | KEY_ALL_ACCESS | KEY_WOW64_64KEY, &hSubkey );
        RegOpenKeyEx( hMain, TEXT("HARDWARE\\ldl\\ldlRegPath"), 0, KEY_ENUMERATE_SUB_KEYS | KEY_ALL_ACCESS | KEY_WOW64_64KEY, &hSsubkey );


        std::wstring wstrName =  strName.toStdWString();
        LPCWSTR chName = wstrName.c_str();

        WCHAR wchRegPath[256] = {};
        DWORD dwSize = 256;
        DWORD dwType = REG_SZ;
        RegQueryValueEx( hSsubkey, chName, NULL, &dwType, (PBYTE)wchRegPath, &dwSize );
        QString strRegPath = QString::fromWCharArray( wchRegPath );
        if( "s" == strRegPath.left(1) )
        {

            hMain2 = HKEY_CURRENT_USER;
        }
        else
        {
            hMain2 = HKEY_LOCAL_MACHINE;
        }


        //qDebug()<<strRegPath;

        WCHAR wchValue[256] = {};
        DWORD dwSize1 = 256;
        RegQueryValueEx( hSubkey, chName, NULL, &dwType, (PBYTE)wchValue, &dwSize1 );
        QString strValue = QString::fromStdWString( wchValue );
        //qDebug()<<strValue;

        LONG lReturn = RegDeleteValue( hSubkey, chName );
        RegDeleteValue( hSsubkey, chName );

        HKEY hSet;
        RegOpenKeyEx( hMain2,wchRegPath , 0, KEY_ALL_ACCESS|KEY_WOW64_64KEY, &hSet );
        RegSetValueEx( hSet, chName, 0, REG_SZ, (PBYTE)wchValue, (strPath.size())*3 );
        /*HKEY hDelete = HKEY_LOCAL_MACHINE;
        HKEY hSub;
        QString strValue = m_strList.at( iRow );
        std::wstring wstrValue =  strValue.toStdWString();
        LPCWSTR chValue = wstrValue.c_str();
        DWORD dwLength =  strValue.size();
        RegOpenKeyEx( hDelete, TEXT("HARDWARE\\ldl"), 0, KEY_ALL_ACCESS|KEY_WOW64_64KEY, &hSub );
        RegSetValueEx( hSub, chName, 0, REG_SZ, (PBYTE)chValue, dwLength*3 );*/

        RegCloseKey( hMain );
        RegCloseKey( hSubkey );
        RegCloseKey( hSsubkey );
        RegCloseKey( hMain2 );
        RegCloseKey( hSet );

    }
}


CWidget::~CWidget()
{
    delete ui;
}
