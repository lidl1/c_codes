#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPalette>
#include <QDebug>
#include <windows.h>
#include <QListWidget>
#include <QListWidgetItem>
#include <QFileIconProvider>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QComboBox>
#include <QSettings>


namespace Ui {
class Widget;
}

class CWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CWidget(QWidget *parent = 0);
    ~CWidget();
    QString     m_strName;


    QIcon        setIcon( QString strFilePath );
    QString      getPath( QString strPath );
    void         makeList( QListWidget* pList, QListWidgetItem* pItem, QString strMsg, QString str, QString strFilePath, DWORD dwStatus, INT index, DWORD dwStartType );
    void         makeStartList( QListWidget* pList, QListWidgetItem* pItem, QString strMsg, QString strIconPath, INT index, BOOL bLaction );
    void         SaoMiaoStart();
    void         SaoMiaoService();
    void         SaoMiaoReg( HKEY hkey, LPCWSTR chPath, QStringList& strList, INT& iIndex, BOOL bLaction );

    QStringList  m_strDeleteList;
    QStringList  m_strList;
    QStringList  m_strPath;

private:
    Ui::Widget *ui;

public slots:
    void SaoMiao();
    void ChangeBySatus( int i );
    void ChangeBySatus1( int i );
};

#endif // WIDGET_H
